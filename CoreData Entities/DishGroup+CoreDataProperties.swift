//
//  DishGroup+CoreDataProperties.swift
//  MelonBook
//
//  Created by Михаил on 28.10.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//
//

import Foundation
import CoreData


extension DishGroup {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<DishGroup> {
        return NSFetchRequest<DishGroup>(entityName: "DishGroup")
    }

    @NSManaged public var dishImageData: Data?
    @NSManaged public var id: UUID?
    @NSManaged public var name: String?
    @NSManaged public var recipes: NSSet?

}

// MARK: Generated accessors for recipes
extension DishGroup {

    @objc(addRecipesObject:)
    @NSManaged public func addToRecipes(_ value: Recipe)

    @objc(removeRecipesObject:)
    @NSManaged public func removeFromRecipes(_ value: Recipe)

    @objc(addRecipes:)
    @NSManaged public func addToRecipes(_ values: NSSet)

    @objc(removeRecipes:)
    @NSManaged public func removeFromRecipes(_ values: NSSet)

}

extension DishGroup : Identifiable {

}
