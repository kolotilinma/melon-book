//
//  Ingredient+CoreDataProperties.swift
//  MelonBook
//
//  Created by Михаил on 28.10.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//
//

import Foundation
import CoreData


extension Ingredient {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Ingredient> {
        return NSFetchRequest<Ingredient>(entityName: "Ingredient")
    }

    @NSManaged public var amount: Int16
    @NSManaged public var name: String?
    @NSManaged public var unit: String?
    @NSManaged public var section: Section?

}

extension Ingredient : Identifiable {

}
