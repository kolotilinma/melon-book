//
//  Recipe+CoreDataProperties.swift
//  MelonBook
//
//  Created by Михаил on 28.10.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//
//

import Foundation
import CoreData


extension Recipe {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Recipe> {
        return NSFetchRequest<Recipe>(entityName: "Recipe")
    }

    @NSManaged public var comment: String?
    @NSManaged public var difficult: Int16
    @NSManaged public var folder: String?
    @NSManaged public var id: UUID?
    @NSManaged public var name: String?
    @NSManaged public var numberOfServings: Int16
    @NSManaged public var recipeImageData: Data?
    @NSManaged public var teg: String?
    @NSManaged public var time: Int16
    @NSManaged public var cookingStage: NSSet?
    @NSManaged public var dishGroup: DishGroup?
    @NSManaged public var sections: NSSet?

}

// MARK: Generated accessors for cookingStage
extension Recipe {

    @objc(addCookingStageObject:)
    @NSManaged public func addToCookingStage(_ value: StepCooking)

    @objc(removeCookingStageObject:)
    @NSManaged public func removeFromCookingStage(_ value: StepCooking)

    @objc(addCookingStage:)
    @NSManaged public func addToCookingStage(_ values: NSSet)

    @objc(removeCookingStage:)
    @NSManaged public func removeFromCookingStage(_ values: NSSet)

}

// MARK: Generated accessors for sections
extension Recipe {

    @objc(addSectionsObject:)
    @NSManaged public func addToSections(_ value: Section)

    @objc(removeSectionsObject:)
    @NSManaged public func removeFromSections(_ value: Section)

    @objc(addSections:)
    @NSManaged public func addToSections(_ values: NSSet)

    @objc(removeSections:)
    @NSManaged public func removeFromSections(_ values: NSSet)

}

extension Recipe : Identifiable {

}
