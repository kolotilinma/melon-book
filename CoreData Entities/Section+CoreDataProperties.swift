//
//  Section+CoreDataProperties.swift
//  MelonBook
//
//  Created by Михаил on 28.10.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//
//

import Foundation
import CoreData


extension Section {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Section> {
        return NSFetchRequest<Section>(entityName: "Section")
    }

    @NSManaged public var name: String?
    @NSManaged public var ingredients: NSSet?
    @NSManaged public var recipe: Recipe?

}

// MARK: Generated accessors for ingredients
extension Section {

    @objc(addIngredientsObject:)
    @NSManaged public func addToIngredients(_ value: Ingredient)

    @objc(removeIngredientsObject:)
    @NSManaged public func removeFromIngredients(_ value: Ingredient)

    @objc(addIngredients:)
    @NSManaged public func addToIngredients(_ values: NSSet)

    @objc(removeIngredients:)
    @NSManaged public func removeFromIngredients(_ values: NSSet)

}

extension Section : Identifiable {

}
