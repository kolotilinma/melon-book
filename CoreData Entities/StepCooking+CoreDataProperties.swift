//
//  StepCooking+CoreDataProperties.swift
//  MelonBook
//
//  Created by Михаил on 28.10.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//
//

import Foundation
import CoreData


extension StepCooking {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<StepCooking> {
        return NSFetchRequest<StepCooking>(entityName: "StepCooking")
    }

    @NSManaged public var number: Int16
    @NSManaged public var stepDescription: String?
    @NSManaged public var stepImageData: Data?
    @NSManaged public var recipe: Recipe?

}

extension StepCooking : Identifiable {

}
