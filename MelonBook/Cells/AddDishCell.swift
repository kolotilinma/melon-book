//
//  DishCell.swift
//  MelonBook
//
//  Created by Евгения Колдышева on 05.09.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

class AddDishCell: UICollectionViewCell {
    
//    var data: DishGroup?{
//        didSet{
//            guard let data = data else {return}
//            title.text = data.name
//            guard let imageData = data.imageData else { return }
//            dish.image = UIImage(data: imageData)
//        }
//    }
    
    fileprivate let bg: UIImageView = {
        let iv = UIImageView()
        iv.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        iv.layer.cornerRadius = 15
        return iv
    }()
    
    fileprivate let dish: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "Salad")
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = .scaleAspectFit
        iv.clipsToBounds = true
        iv.alpha = 0.15
        return iv
    }()
    
    fileprivate let symbol: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "Rectangle 41")
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = .scaleAspectFit
        iv.clipsToBounds = true
        return iv
    }()
    
    fileprivate let title: UILabel = {
        let title = UILabel(text: "+ Добавить папку", font: .ahellya(15), color: #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1))
        title.translatesAutoresizingMaskIntoConstraints = false
        title.contentMode = .scaleAspectFit
        return title
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.addSubview(bg)
        bg.topAnchor.constraint(equalTo: self.topAnchor, constant: 10).isActive = true
        bg.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        bg.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        bg.heightAnchor.constraint(equalTo: self.heightAnchor,multiplier: 0.8).isActive = true
        
        bg.addSubview(dish)
        dish.centerXAnchor.constraint(equalTo: bg.centerXAnchor).isActive = true
        dish.topAnchor.constraint(equalTo: bg.topAnchor, constant: 5).isActive = true
        dish.bottomAnchor.constraint(equalTo: bg.bottomAnchor, constant: -5).isActive = true
        
        bg.addSubview(symbol)
        symbol.centerXAnchor.constraint(equalTo: bg.centerXAnchor).isActive = true
        symbol.topAnchor.constraint(equalTo: bg.topAnchor, constant: 28).isActive = true
        symbol.bottomAnchor.constraint(equalTo: bg.bottomAnchor, constant: -40).isActive = true
        
        contentView.addSubview(title)
        title.topAnchor.constraint(equalTo: bg.bottomAnchor, constant: 10).isActive = true
        title.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
