//
//  AddRecipeCell.swift
//  MelonBook
//
//  Created by Евгения Колдышева on 20.09.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

class AddRecipeCell: UICollectionViewCell {
    
    var data: RecipeOld?{
        didSet{
            guard let data = data else { return }
            titleLabel.text = data.name
            guard let imageString = data.titleImage else { return }
            guard let image = UIImage(named: imageString) else { return }
            dishImageView.image = image
        }
    }
    
    fileprivate let bgImageView: UIImageView = {
        let iv = UIImageView()
        iv.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        iv.layer.cornerRadius = 15
        return iv
    }()
    
    fileprivate let dishImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "Salad")
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = .scaleAspectFit
        iv.clipsToBounds = true
        iv.alpha = 0.15
        return iv
    }()
    
    fileprivate let symbolImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "Rectangle 41")
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = .scaleAspectFit
        iv.clipsToBounds = true
        return iv
    }()
    
    fileprivate let titleLabel: UILabel = {
        let title = UILabel(text: "+ Добавить рецепт", font: .ahellya(15), color: #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1))
        title.translatesAutoresizingMaskIntoConstraints = false
        title.contentMode = .scaleAspectFit
        return title
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.addSubview(bgImageView)
        bgImageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 15).isActive = true
        bgImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        bgImageView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
//        bg.heightAnchor.constraint(equalTo: self.heightAnchor,multiplier: 0.7).isActive = true
        bgImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true

        bgImageView.addSubview(dishImageView)
        dishImageView.centerXAnchor.constraint(equalTo: bgImageView.centerXAnchor).isActive = true
        dishImageView.topAnchor.constraint(equalTo: bgImageView.topAnchor).isActive = true
        dishImageView.bottomAnchor.constraint(equalTo: bgImageView.bottomAnchor, constant: -2).isActive = true
        
        bgImageView.addSubview(symbolImageView)
        symbolImageView.centerXAnchor.constraint(equalTo: bgImageView.centerXAnchor).isActive = true
        symbolImageView.topAnchor.constraint(equalTo: bgImageView.topAnchor, constant: 55).isActive = true
        symbolImageView.bottomAnchor.constraint(equalTo: bgImageView.bottomAnchor, constant: -55).isActive = true
        
        contentView.addSubview(titleLabel)
        titleLabel.topAnchor.constraint(equalTo: bgImageView.bottomAnchor).isActive = true
        titleLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
