//
//  CreateIngredientCell.swift
//  MelonBook
//
//  Created by Евгения Колдышева on 28.09.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit
//import PickerTextField

class CreateIngredientCell: UITableViewCell {
    
    var delegate: ItemTableViewCellDelegate?
    var ingridient: IngredientOld? {
        didSet {
            guard let ingridient = ingridient else {return}
            ingredientTextField.text = ingridient.name
            if ingridient.amount != nil {
                countTextField.text = String(ingridient.amount!)
            }
            unitTextField.text = ingridient.unit
        }
    }
    var ingredientTextField: UITextField = {
        let textField = UITextField()
        textField.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        textField.font = .ahellya(15)
        textField.textColor = UIColor(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1)
        textField.layer.cornerRadius = 12
        textField.contentVerticalAlignment = .center
        textField.textAlignment = .justified
        textField.setLeftPaddingPoints(10)
        return textField
    }()
    
     var countTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Кол-во"
        textField.keyboardType = .numberPad
        textField.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        textField.font = .ahellya(15)
        textField.textColor = UIColor(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1)
        textField.layer.cornerRadius = 12
        textField.contentVerticalAlignment = .center
        textField.textAlignment = .center
        return textField
    }()
    
    var unitTextField: PickerTextField = {
        let textField = PickerTextField()
        textField.placeholder = "Ед."
        textField.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        textField.keyboardToolbar.doneBarButton.tintColor = UIColor(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1)
//        textField.textColor = UIColor(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1)
        textField.font = .ahellya(15)
        textField.textColor = UIColor(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1)
        textField.layer.cornerRadius = 12
        textField.contentVerticalAlignment = .center
        textField.textAlignment = .center
        return textField
    }()
    
    fileprivate var lineImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image =  #imageLiteral(resourceName: "Rectangle 97")
        imageView.clipsToBounds = true
        return imageView
    }()
    
    fileprivate var threeLinesImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image =  #imageLiteral(resourceName: "Rectangle 100")
        imageView.clipsToBounds = true
        return imageView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = UIColor(red: 0.933, green: 0.933, blue: 0.933, alpha: 1)
        unitTextField.keyboardToolbar.doneBarButton.tintColor = UIColor(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1)
//        contentView.addSubview(threeLinesImageView)
//        threeLinesImageView.centerY(inView: self)
//        threeLinesImageView.anchor(right: rightAnchor, width: 32, height: 29)
        
        contentView.addSubview(unitTextField)
        unitTextField.centerY(inView: self)
        unitTextField.anchor(right: rightAnchor, paddingRight: 52, width: 60, height: 45)
        
        contentView.addSubview(countTextField)
        countTextField.centerY(inView: unitTextField)
        countTextField.anchor(right: unitTextField.leftAnchor, paddingRight: 10, width: 60, height: 45)
        
        contentView.addSubview(lineImageView)
        lineImageView.centerY(inView: countTextField)
        lineImageView.anchor(right: countTextField.leftAnchor, paddingRight: 5, width: 27, height: 21)
        
        contentView.addSubview(ingredientTextField)
        ingredientTextField.centerY(inView: lineImageView)
        ingredientTextField.anchor(left: leftAnchor, right: lineImageView.leftAnchor, paddingLeft: 25, paddingRight: 5, height: 45)
        
        ingredientTextField.delegate = self
        countTextField.delegate = self
        unitTextField.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func clearAllFields() {
        self.unitTextField.text = ""
        self.countTextField.text = ""
        self.ingredientTextField.text = ""
    }
}

extension CreateIngredientCell: UITextFieldDelegate {
    
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        NewRecipeViewController().textFieldDidBeginEditing(textField)
//    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        delegate?.textFieldDidEndEditing(textField: textField, cell: self)
    }
    
}

