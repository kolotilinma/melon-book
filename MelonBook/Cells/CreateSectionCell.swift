//
//  SectionCell.swift
//  MelonBook
//
//  Created by Евгения Колдышева on 28.09.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

class CreateSectionCell: UITableViewCell, UITextFieldDelegate {
    
     var sectionNameTextField: UITextField = {
        let textField = UITextField()
        textField.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        textField.font = .ahellya(15)
        textField.textColor = UIColor(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1)
        textField.layer.cornerRadius = 12
        textField.contentVerticalAlignment = .center
        textField.textAlignment = .justified
        textField.setLeftPaddingPoints(10)
        return textField
    }()
    
    fileprivate var dotsImageView = UIImageView(image: #imageLiteral(resourceName: "Rectangle 95"))
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = UIColor(red: 0.933, green: 0.933, blue: 0.933, alpha: 1)

        contentView.addSubview(sectionNameTextField)
        sectionNameTextField.anchor(top: topAnchor, left: leftAnchor, right: rightAnchor, bottom: bottomAnchor, paddingTop: 20, paddingBottom: 20, height: 46)
        
        sectionNameTextField.addSubview(dotsImageView)
        dotsImageView.anchor(top: sectionNameTextField.topAnchor, right: sectionNameTextField.rightAnchor, paddingTop: 15, paddingRight: 10, width: 21, height: 18)
        
        sectionNameTextField.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func clearField() {
        self.sectionNameTextField.text = ""
    }
}
