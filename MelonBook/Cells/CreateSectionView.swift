//
//  CreateSectionView.swift
//  MelonBook
//
//  Created by Михаил on 28.10.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

class CreateSectionView: UIView {
    
    var delegate: ItemTableViewCellDelegate?
    var section: Int?
    var sectionNameTextField: UITextField = {
        let textField = UITextField()
        textField.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        textField.font = .ahellya(15)
        textField.textColor = UIColor(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1)
        textField.layer.cornerRadius = 12
        textField.contentVerticalAlignment = .center
        textField.textAlignment = .justified
        textField.setLeftPaddingPoints(10)
        return textField
    }()
    
//    fileprivate var dotsImageView = UIImageView(image: #imageLiteral(resourceName: "Rectangle 95"))
    
    init() {
        super.init(frame: .zero)
        self.backgroundColor = UIColor(red: 0.933, green: 0.933, blue: 0.933, alpha: 1)
        sectionNameTextField.delegate = self
        
        addSubview(sectionNameTextField)
        sectionNameTextField.centerY(inView: self)
        sectionNameTextField.anchor(left: leftAnchor, right: rightAnchor,
                                    paddingLeft: 25, paddingRight: 25, height: 46)
        
//        sectionNameTextField.addSubview(dotsImageView)
//        dotsImageView.centerY(inView: sectionNameTextField)
//        dotsImageView.anchor(right: sectionNameTextField.rightAnchor, paddingRight: 10, width: 21, height: 18)
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func clearField() {
        self.sectionNameTextField.text = ""
    }
}

extension CreateSectionView: UITextFieldDelegate {
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        delegate?.textFieldDidEndEditing(textField: textField, indexSection: section ?? 0)
        
    }
}
