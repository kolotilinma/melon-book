//
//  DishCell.swift
//  MelonBook
//
//  Created by Евгения Колдышева on 05.09.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

class DishCell: UICollectionViewCell {
    
    var data: DishGroup?{
        didSet{
            guard let data = data else {return}
            dishNameLabel.text = "\(data.name ?? "") (\(data.recipes?.count ?? 0))"
            guard let imageData = data.dishImageData else { return }
            dishImageView.image = UIImage(data: imageData)
        }
    }
    weak var delegate: selectViewCellDelegate? = nil
    
    fileprivate let dishImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = #colorLiteral(red: 0.9607843137, green: 0.9607843137, blue: 0.9607843137, alpha: 1)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 15
        return imageView
    }()
    
    fileprivate let dishNameLabel = UILabel(text: "", font: .ahellya(15), color: #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1))
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.addSubview(dishImageView)
        dishImageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 10).isActive = true
        dishImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        dishImageView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        dishImageView.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.8).isActive = true
        
        contentView.addSubview(dishNameLabel)
        dishNameLabel.centerX(inView: self, topAnchor: dishImageView.bottomAnchor, paddingTop: 10)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
