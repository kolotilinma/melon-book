//
//  FolderCell.swift
//  MelonBook
//
//  Created by Евгения Колдышева on 16.10.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

class FolderCell: UITableViewCell, UITextFieldDelegate {
    
    let folderLabel: UILabel = {
        let label = UILabel(text: "Default", font: .ahellya(15), color: #colorLiteral(red: 0.768627451, green: 0.768627451, blue: 0.768627451, alpha: 1))
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    var dotImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "Rectangle 279")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.clipsToBounds = true

        return imageView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = .white

        contentView.addSubview(folderLabel)
        folderLabel.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, paddingTop: 7, paddingLeft: 15, paddingBottom: 7, height: 30)

        contentView.addSubview(dotImageView)
        dotImageView.anchor(right: rightAnchor, paddingRight: 20, width: 10, height: 11)
        dotImageView.centerY(inView: self)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func selectCell() {
        folderLabel.textColor = #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1)
        dotImageView.image = #imageLiteral(resourceName: "Rectangle 278")
    }
    
    func deselectCell() {
        folderLabel.textColor = #colorLiteral(red: 0.768627451, green: 0.768627451, blue: 0.768627451, alpha: 1)
        dotImageView.image = #imageLiteral(resourceName: "Rectangle 279")
    }
}
