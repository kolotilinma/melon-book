//
//  RecipeCell.swift
//  MelonBook
//
//  Created by Евгения Колдышева on 19.09.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

class RecipeCell: UICollectionViewCell, SelfConfiguratingCell {
    
    static var reuseId: String = "RecipeCell"
        
    fileprivate let dishImageView: UIImageView = {
        let iv = UIImageView()
        iv.backgroundColor = .lightGray
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = .scaleAspectFill
        iv.layer.cornerRadius = 15
        iv.clipsToBounds = true
        return iv
    }()
    
    fileprivate let titleLabel = UILabel(text: "", font: .ahellya(15), color: #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1))
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.addSubview(dishImageView)
        dishImageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 10).isActive = true
        dishImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        dishImageView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        dishImageView.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.8).isActive = true
                
        contentView.addSubview(titleLabel)
        titleLabel.centerX(inView: self, topAnchor: dishImageView.bottomAnchor, paddingTop: 5)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure<U>(with value: U) where U : Hashable {
        guard let data: Recipe = value as? Recipe else { return }
        titleLabel.text = data.name
        guard let imageData = data.recipeImageData else { return }
        guard let image = UIImage(data: imageData) else { return }
        dishImageView.image = image
    }
    
}
