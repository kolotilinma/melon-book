//
//  CookingStageTableViewCell.swift
//  MelonBook
//
//  Created by Михаил on 25.09.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

class CookingStageTableViewCell: UITableViewCell {

    //MARK: - Properties
    var step: StepCooking? {
        didSet { configure() }
    }
    
    private let stepLabel = UILabel(text: "Шаг ", font: .ahellya(15), color: #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1))
    private let recipeStepLabel = UILabel(text: "Recipe step description", font: .ahellya(15), color: #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1))
    
    private lazy var stepImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.layer.cornerRadius = 15
        imageView.clipsToBounds = true
//        imageView.image = #imageLiteral(resourceName: "Rectangle 83")
        imageView.backgroundColor = #colorLiteral(red: 0.921431005, green: 0.9214526415, blue: 0.9214410186, alpha: 1)
        return imageView
    }()
    
    //MARK: - Lifecycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        recipeStepLabel.numberOfLines = 0
        addSubview(stepLabel)
        stepLabel.anchor(top: self.topAnchor, left: self.leftAnchor, right: self.rightAnchor,
                         paddingTop: 10, paddingLeft: 20, paddingRight: 20)
        addSubview(recipeStepLabel)
        recipeStepLabel.anchor(top: stepLabel.bottomAnchor, left: self.leftAnchor, right: self.rightAnchor,
                               paddingTop: 20, paddingLeft: 20, paddingRight: 20)
        addSubview(stepImageView)
        stepImageView.anchor(top: recipeStepLabel.bottomAnchor, left: self.leftAnchor, right: self.rightAnchor,
                             paddingTop: 20, paddingLeft: 20, paddingRight: 90)
        stepImageView.heightAnchor.constraint(equalTo: stepImageView.widthAnchor, multiplier: 0.65).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Helpers
    func configure() {
        guard let step = step else { return }
        stepLabel.text = "Шаг \(step.number)"
        recipeStepLabel.text = step.stepDescription
        guard let imagedata = step.stepImageData else { return }
        stepImageView.image = UIImage(data: imagedata)
    }
    
    

}
