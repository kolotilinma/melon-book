//
//  FooterRecipeTableViewCell.swift
//  MelonBook
//
//  Created by Михаил on 25.09.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

class FooterRecipeTableViewCell: UITableViewCell {

    //MARK: - Properties
    var recipe: Recipe? {
        didSet { configure() }
    }
    
    let commentsLabel = UILabel(text: "Comments....", font: .ahellya(15), color: #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1))
    let folderLabel = UILabel(text: "Folder", font: .ahellya(15), color: #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1))
    
    private lazy var stepImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.layer.cornerRadius = 15
        imageView.clipsToBounds = true
        imageView.image = #imageLiteral(resourceName: "Rectangle 83")
        imageView.backgroundColor = #colorLiteral(red: 0.921431005, green: 0.9214526415, blue: 0.9214410186, alpha: 1)
        return imageView
    }()
    
    //MARK: - Lifecycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commentsLabel.numberOfLines = 0
        let stack = UIStackView(arrangedSubviews: [commentsLabel, folderLabel], axis: .vertical, spacing: 20)
        addSubview(stack)
        stack.anchor(left: self.leftAnchor, right: self.rightAnchor, paddingLeft: 20, paddingRight: 20)
        stack.centerX(inView: self, topAnchor: self.topAnchor, paddingTop: 20)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Helpers
    func configure() {
        guard let recipe = recipe else { return }
        commentsLabel.text = recipe.comment
        folderLabel.text = "Папка: \(recipe.dishGroup?.name ?? "")"
    }
    
    

}
