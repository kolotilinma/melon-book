//
//  IngridientCell.swift
//  MelonBook
//
//  Created by Михаил on 25.09.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

class IngridientCell: UITableViewCell {
    
    //MARK: - Properties
    var ingredient: Ingredient? {
        didSet { configure() }
    }
    private let nameLabel = UILabel(text: "Ingridient Name", font: .ahellya(15), color: #colorLiteral(red: 0.5960784314, green: 0.5960784314, blue: 0.5960784314, alpha: 1))
    private let amountLabel = UILabel(text: "0", font: .ahellya(15), color: #colorLiteral(red: 0.5960784314, green: 0.5960784314, blue: 0.5960784314, alpha: 1))
    private let unitLabel = UILabel(text: "unit", font: .ahellya(15), color: #colorLiteral(red: 0.5960784314, green: 0.5960784314, blue: 0.5960784314, alpha: 1))
    
    //MARK: - Lifecycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(nameLabel)
        nameLabel.centerY(inView: self, leftAnchor: self.leftAnchor, paddingLeft: 20)
        addSubview(unitLabel)
        unitLabel.anchor(right: self.rightAnchor, paddingRight: 50)
        unitLabel.centerY(inView: self)
        addSubview(amountLabel)
        amountLabel.anchor(right: unitLabel.leftAnchor, paddingRight: 15)
        amountLabel.centerY(inView: unitLabel)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //MARK: - Helpers
    func configure() {
        guard let ingredient = ingredient else { return }
        nameLabel.text = ingredient.name
        unitLabel.text = ingredient.unit
        amountLabel.text = "\(ingredient.amount)"
    }
}
