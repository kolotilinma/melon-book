//
//  RecipeTableViewCell.swift
//  MelonBook
//
//  Created by Михаил on 25.09.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

private let ingridientReuseIdentifier = "IngridientCell"

class RecipeTableViewCell: UITableViewCell {
    
    //MARK: - Properties
    var sectionCooking: Section? {
        didSet {
            configure()
            guard let allIngredients = sectionCooking?.ingredients?.allObjects as? [Ingredient] else { return }
            ingredients = allIngredients
        }
    }
    var ingredients: [Ingredient]?
    
    let tableView = UITableView()
    private let sectionNameLabel = UILabel(text: "Section Name", font: .ahellya(15), color: #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1))
    
    
    //MARK: - Lifecycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupTableView()
        setupUI()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Helpers
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        tableView.separatorStyle = .none
        tableView.register(IngridientCell.self, forCellReuseIdentifier: ingridientReuseIdentifier)
    }
    func setupUI() {
        overrideUserInterfaceStyle = .light
        addSubview(sectionNameLabel)
        sectionNameLabel.anchor(top: self.topAnchor, left: self.leftAnchor, right: self.rightAnchor,
                                paddingTop: 10, paddingLeft: 20)
        let separatopView = UIView()
        separatopView.backgroundColor = #colorLiteral(red: 0.768627451, green: 0.768627451, blue: 0.768627451, alpha: 0.2983225639)
        addSubview(separatopView)
        separatopView.anchor(top: sectionNameLabel.bottomAnchor, left: self.leftAnchor, right: self.rightAnchor,
                             paddingTop: 10, paddingLeft: 20, paddingRight: 20, height: 2)
        addSubview(tableView)
        tableView.anchor(top: separatopView.bottomAnchor, left: self.leftAnchor,
                         right: self.rightAnchor, bottom: self.bottomAnchor)
    }
    
    func configure() {
        guard let sectionCooking = sectionCooking else { return }
        sectionNameLabel.text = sectionCooking.name
    }
    

}

extension RecipeTableViewCell: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let numberOfSectionCooking = sectionCooking?.ingredients?.count else { return 0 }
        return numberOfSectionCooking
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ingridientReuseIdentifier, for: indexPath) as! IngridientCell
        cell.ingredient = ingredients?[indexPath.row]
        return cell
    }
    
    
}
