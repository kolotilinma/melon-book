//
//  RecipeTitleCell.swift
//  MelonBook
//
//  Created by Михаил on 25.09.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

class RecipeTitleCell: UITableViewCell {
    
    //MARK: - Properties
    var recipe: Recipe? {
        didSet { configure() }
    }
    
    private lazy var titleImageView = TitleRecipeImageView(title: "Загрузите фото", image: #imageLiteral(resourceName: "NewFolderBackground"), iconsIsHidden: true)
    private let recipeNameLabel = UILabel(text: "", font: .ahellya(30), color: #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1))
    let numOfServView = RecipeHeaderElementView(text: "порции", image: #imageLiteral(resourceName: "Rectangle 91"), value: "")
    let timeServView = RecipeHeaderElementView(text: "минут", image: #imageLiteral(resourceName: "Rectangle 92"), value: "")
    let difficultyView = RecipeHeaderElementView(text: "сложность", image: #imageLiteral(resourceName: "Rectangle 93"), value: "")
    private let tagsLabel = UILabel(text: "", font: .ahellya(15), color: #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1))
    
    //MARK: - Lifecycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        recipeNameLabel.numberOfLines = 2
        tagsLabel.numberOfLines = 2
        addSubview(titleImageView)
        titleImageView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        titleImageView.anchor(top: self.topAnchor, left: self.leftAnchor, right: self.rightAnchor,
                              paddingLeft: 13, paddingRight: 13)
        
        let stackView = UIStackView(arrangedSubviews: [numOfServView, timeServView, difficultyView],
                                    axis: .horizontal, spacing: 20)
        stackView.distribution = .fillEqually
        
        let stack = UIStackView(arrangedSubviews: [recipeNameLabel, stackView, tagsLabel], axis: .vertical, spacing: 10)
        stack.distribution = .fillEqually
        addSubview(stack)
        stack.anchor(left: self.leftAnchor, right: self.rightAnchor,
                     paddingLeft: 20, paddingRight: 20, height: 200)
        stack.centerX(inView: self, topAnchor: titleImageView.bottomAnchor, paddingTop: 20)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Helpers
    func configure() {
        guard let recipe = recipe else { return }
        recipeNameLabel.text = recipe.name
        tagsLabel.text = recipe.teg
        guard let imageData = recipe.recipeImageData else { return }
        guard let image = UIImage(data: imageData) else { return }
        titleImageView.imageView.image = image
        titleImageView.imageView.alpha = 1
        numOfServView.valueLabel.text = String(recipe.numberOfServings)
        timeServView.valueLabel.text = String(recipe.time)
        difficultyView.valueLabel.text = "\(String(recipe.difficult))/3"
    }
    

}
