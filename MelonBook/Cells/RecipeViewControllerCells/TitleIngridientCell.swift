//
//  TitleIngridientCell.swift
//  MelonBook
//
//  Created by Михаил on 27.09.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

class TitleIngridientCell: UITableViewCell {
    
    //MARK: - Properties
    var titleName: String? {
        didSet { configure() }
    }
    
    let titleLabel = UILabel(text: "", font: .ahellya(23), color:  #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1))
    
    //MARK: - Lifecycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        addSubview(titleLabel)
        titleLabel.centerY(inView: self, leftAnchor: self.leftAnchor, paddingLeft: 20)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Helpers
    func configure() {
        titleLabel.text = titleName
    }
    

}
