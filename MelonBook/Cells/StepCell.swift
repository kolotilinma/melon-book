//
//  StepCell.swift
//  MelonBook
//
//  Created by Евгения Колдышева on 16.10.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

class StepCell: UITableViewCell {

    var delegate : CellImageTapDelegate?
    var tapGestureRecognizer = UITapGestureRecognizer()
    let stepLabel: UILabel = {
        let label = UILabel(text: "Шаг 1", font: .ahellya(15), color: #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1))
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let cameraImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "Rectangle 111")
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 7
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    var placeholder = UILabel(text: "Опишите шаг:", font: .ahellya(15), color: .placeholderText)
    var stepTextView: UITextView = {
        let textView = UITextView()
        textView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        textView.layer.cornerRadius = 12
        textView.layer.borderWidth = 3
        textView.text = ""
        textView.layer.borderColor = UIColor(red: 0.933, green: 0.933, blue: 0.933, alpha: 1).cgColor
        textView.textContainerInset = UIEdgeInsets(top: 10, left: 5, bottom: 5, right: 5)
        textView.font = .ahellya(15)
        textView.textColor = #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1)
        textView.textAlignment = .left
        return textView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = UIColor(red: 0.933, green: 0.933, blue: 0.933, alpha: 1)
        setupConstraints()
        stepTextView.delegate = self
        tapGestureRecognizer.addTarget(self, action: #selector(StepCell.imageTapped(gestureRecgonizer:)))
        cameraImageView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupConstraints() {
        contentView.addSubview(stepLabel)
        stepLabel.anchor(top: topAnchor, left: leftAnchor, right: rightAnchor, paddingTop: 5)
        
        contentView.addSubview(cameraImageView)
        cameraImageView.anchor(top: stepLabel.bottomAnchor, left: leftAnchor, paddingTop: 18, width: 34, height: 30)
        
        contentView.addSubview(stepTextView)
        stepTextView.anchor(top: stepLabel.bottomAnchor, left: cameraImageView.rightAnchor, right: rightAnchor, bottom: bottomAnchor, paddingTop: 15, paddingLeft: 15, paddingBottom: 5, height: 64)
        
        contentView.addSubview(placeholder)
        placeholder.anchor(top: stepTextView.topAnchor, left: stepTextView.leftAnchor,
                           paddingTop: 12, paddingLeft: 10)
    }
    
    func clearField() {
        self.stepTextView.text = ""
        self.cameraImageView.image = #imageLiteral(resourceName: "Rectangle 111")
    }
    
    @objc func imageTapped(gestureRecgonizer: UITapGestureRecognizer) {
        delegate?.tableCell(didClickedImageOf: self)
    }
    
    @objc func imageTaped()  {
        guard let controller = UIApplication.topViewController() else { return }
        ImagePickerManager().pickImage(controller){ image in
            self.cameraImageView.image = image
        }
    }
}

extension StepCell: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            placeholder.isHidden = true
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            placeholder.isHidden = false
        }
    }
}
