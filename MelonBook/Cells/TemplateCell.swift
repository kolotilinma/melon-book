//
//  TemplateCell.swift
//  MelonBook
//
//  Created by Евгения Колдышева on 21.09.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

class TemplateCell: UICollectionViewCell {
    
    var imageString: String?{
        didSet {
            guard let data = imageString else { return }
            guard let image = UIImage(named: data) else { return }
            dishImageView.image = image
        }
    }
    weak var delegate: selectViewCellDelegate? = nil
    
    fileprivate let bgImageView: UIImageView = {
        let iv = UIImageView()
        iv.backgroundColor = #colorLiteral(red: 0.9607843137, green: 0.9607843137, blue: 0.9607843137, alpha: 1)
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = .scaleAspectFit
        iv.clipsToBounds = true
        iv.layer.cornerRadius = 15
        
        return iv
    }()
    
    fileprivate let dishImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "Salad")
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = .scaleAspectFit
        iv.clipsToBounds = true
        return iv
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.addSubview(bgImageView)
        bgImageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 10).isActive = true
        bgImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        bgImageView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        bgImageView.heightAnchor.constraint(equalTo: self.heightAnchor,multiplier: 0.8).isActive = true
        
        bgImageView.addSubview(dishImageView)
        dishImageView.centerXAnchor.constraint(equalTo: bgImageView.centerXAnchor).isActive = true
        dishImageView.topAnchor.constraint(equalTo: bgImageView.topAnchor, constant: 5).isActive = true
        dishImageView.bottomAnchor.constraint(equalTo: bgImageView.bottomAnchor, constant: -5).isActive = true
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        bgImageView.isUserInteractionEnabled = true
        bgImageView.addGestureRecognizer(gesture)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
 
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer){
        self.delegate?.didSelectItem(image: dishImageView.image!)
    }
    
}
