//
//  SelfConfiguratingCell.swift
//  MelonBook
//
//  Created by Михаил on 17.10.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

protocol SelfConfiguratingCell {
    static var reuseId: String { get }
    func configure<U: Hashable>(with value: U)
}
