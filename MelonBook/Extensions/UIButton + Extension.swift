//
//  UIButton + Extension.swift
//  MelonBook
//
//  Created by Михаил on 17.10.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    convenience init(title: String,
                     titleColor: UIColor? =  #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1),
                     backgroundColor: UIColor? = .white,
                     isShadow: Bool? = true,
                     font: UIFont? = .ahellya(15),
                     cornerRadius: CGFloat = 20) {
        self.init(type: .system)
        self.setTitle(title, for: .normal)
        self.setTitleColor(titleColor, for: .normal)
        self.backgroundColor = backgroundColor
        self.titleLabel?.font = font
        self.layer.cornerRadius = cornerRadius
        self.layer.borderWidth = 1
        self.layer.borderColor = #colorLiteral(red: 0.8745098039, green: 0.8705882353, blue: 0.8705882353, alpha: 1)
        self.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        self.titleEdgeInsets.left = 55
        
        if isShadow ?? true {
            self.layer.shadowColor = UIColor.black.cgColor
            self.layer.shadowRadius = 4
            self.layer.shadowOpacity = 0.2
            self.layer.shadowOffset = CGSize(width: 0, height: 4)
        }
        
    }
    
    convenience init(title: String,
                     titleColor: UIColor? =  #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1),
                     backgroundColor: UIColor? = .white,
                     font: UIFont? = .ahellya(13),
                     cornerRadius: CGFloat = 12) {
        self.init(type: .system)
//        self.init()
        self.setTitle(title, for: .normal)
        self.setTitleColor(titleColor, for: .normal)
        self.backgroundColor = backgroundColor
        self.titleLabel?.font = font
        self.layer.cornerRadius = cornerRadius
//        self.addTarget(self, action: action, for: .touchUpInside)
    }
    
    convenience init(image: UIImage,
                     tintColor: UIColor? = #colorLiteral(red: 0.7371357679, green: 0.732755363, blue: 0.7405038476, alpha: 1)) {
        self.init(type: .system)
        
        self.setImage(image.withRenderingMode(.alwaysTemplate), for: .normal)
        self.tintColor = tintColor
    }
    
    func customizeButton(with image: UIImage) {
        let googleLogo = UIImageView(image: image, contentMode: .scaleAspectFit)
        self.addSubview(googleLogo)
        googleLogo.anchor(width: 30, height: 30)
        googleLogo.centerY(inView: self, leftAnchor: self.leftAnchor, paddingLeft: 14)
    }
    
}
