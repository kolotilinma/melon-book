//
//  UIFont + Extension.swift
//  MelonBook
//
//  Created by Евгения Колдышева on 05.09.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

extension UIFont {
    
    static func hiragano(_ size: CGFloat) -> UIFont? {
        return UIFont.init(name: "Hiragino Mincho ProN W3", size: size)
    }
    
    static func ahellya(_ size: CGFloat) -> UIFont? {
        return UIFont.init(name: "ahellya", size: size)
    }
    
}
