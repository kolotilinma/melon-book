//
//  UIImageView + Extension.swift
//  MelonBook
//
//  Created by Михаил on 16.10.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

extension UIImageView {
    
    convenience init(image: UIImage?, contentMode: UIView.ContentMode) {
        self.init()
        
        self.image = image
        self.contentMode = contentMode
    }
    
    func setupColor(color: UIColor) {
        let tempImage = self.image?.withRenderingMode(.alwaysTemplate)
        self.image = tempImage
        self.tintColor = color
    }
    
}
