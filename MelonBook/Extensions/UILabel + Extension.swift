//
//  UILabel + Extension.swift
//  MelonBook
//
//  Created by Евгения Колдышева on 05.09.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

extension UILabel {
    
    convenience init(text: String, font: UIFont? = .ahellya(15), color: UIColor) {
        self.init()
        
        self.text = text
        self.font = font
        self.textColor = color
    }
}
