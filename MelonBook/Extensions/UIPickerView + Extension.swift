//
//  UIPickerView + Extension.swift
//  MelonBook
//
//  Created by Михаил on 24.10.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

extension UIPickerView {
    
    convenience init(color: UIColor) {
        self.init()
        backgroundColor = color
        isHidden = true
    }
    
}
