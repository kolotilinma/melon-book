//
//  UITableView + override methods.swift
//  MelonBook
//
//  Created by Евгения Колдышева on 16.10.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

final class ContentSizedTableView: UITableView {
    override var contentSize:CGSize {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }

    override var intrinsicContentSize: CGSize {
        layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height)
    }
}
