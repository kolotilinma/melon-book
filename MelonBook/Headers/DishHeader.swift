//
//  DishHeader.swift
//  MelonBook
//
//  Created by Евгения Колдышева on 08.09.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import Foundation
import UIKit

class DishHeader: UICollectionViewCell {
    
    static let reuseId = "DishHeader"
    let title = UILabel(text: "Melon Book",font: .hiragano(30), color: #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1))
    
    fileprivate let backgroundImageView: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "MainBackground"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(backgroundImageView)
        NSLayoutConstraint.activate([
            backgroundImageView.topAnchor.constraint(equalTo: self.topAnchor),
            backgroundImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: -20),
            backgroundImageView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 20),
            backgroundImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
        
        addSubview(title)
        title.centerX(inView: self, topAnchor: self.bottomAnchor, paddingTop: -170)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
