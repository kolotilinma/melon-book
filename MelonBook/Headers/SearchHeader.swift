//
//  SearchHeader.swift
//  MelonBook
//
//  Created by Михаил on 16.10.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import Foundation
import UIKit
import CoreData

protocol SearchHeaderDelegate: NSObjectProtocol {
    func updateRecipes(with searchText: String?)
}

class SearchHeader: UICollectionViewCell {
    
    static let reuseId = "SearchHeader"
    weak var delegate: SearchHeaderDelegate?
        
    fileprivate let recipeNameTextField: CustomTextField  = {
        let textField = CustomTextField()
        textField.placeholder = "Название рецепта:"
        return textField
    }()
    
    fileprivate let recipeIngridientTextField: CustomTextField  = {
        let textField = CustomTextField()
        textField.placeholder = "Название ингредиента:"
        return textField
    }()
    
    fileprivate lazy var searchButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        button.setTitle("Поиск", for: .normal)
        button.titleLabel?.font = .ahellya(15)
        button.tintColor = .white
        button.layer.cornerRadius = 24
        button.addTarget(self, action: #selector(searchButtonTapped), for: .touchUpInside)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
        recipeNameTextField.delegate = self
        recipeIngridientTextField.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        let findRecipeLabel = UILabel(text: "Найти рецепт", font: .ahellya(28), color: #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1))
//        let searchLabel = UILabel(text: "Поиск", font: .ahellya(23), color: #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1))
//        let ingridientLabel = UILabel(text: "Ингредиенты:", font: .ahellya(size: 23), color: #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1))
//        let resultRecipeLabel = UILabel(text: "Результаты поиска", font: .ahellya(28), color: #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1))
        
        addSubview(findRecipeLabel)
        findRecipeLabel.centerX(inView: self, topAnchor: self.safeAreaLayoutGuide.topAnchor, paddingTop: 30)
//        addSubview(searchLabel)
//        searchLabel.anchor(top: findRecipeLabel.bottomAnchor, left: self.leftAnchor,
//                           paddingTop: 40, paddingLeft: 20)
        addSubview(recipeNameTextField)
        recipeNameTextField.anchor(top: findRecipeLabel.bottomAnchor, left: self.leftAnchor, right: self.rightAnchor,
                                   paddingTop: 30, paddingLeft: 20, paddingRight: 20, height: 50)
//        addSubview(ingridientLabel)
//        ingridientLabel.anchor(top: recipeNameTextField.bottomAnchor, left: self.leftAnchor,
//                               paddingTop: 50, paddingLeft: 20)
//        addSubview(recipeIngridientTextField)
//        recipeIngridientTextField.anchor(top: ingridientLabel.bottomAnchor, left: self.leftAnchor, right: self.rightAnchor,
//                                         paddingTop: 30, paddingLeft: 20, paddingRight: 20, height: 50)
//        addSubview(searchButton)
//        searchButton.anchor(top: recipeIngridientTextField.bottomAnchor, paddingTop: 60, width: 200, height: 45)
//        searchButton.centerX(inView: self)
//        addSubview(resultRecipeLabel)
//        resultRecipeLabel.centerX(inView: self, topAnchor: recipeNameTextField.bottomAnchor, paddingTop: 40)
    }
    
    
    @objc func searchButtonTapped() {

    }
    
}

extension SearchHeader: UITextFieldDelegate {
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if textField == recipeNameTextField {
            self.delegate?.updateRecipes(with: recipeNameTextField.text!)
        }
        if textField == recipeIngridientTextField {
            print(textField.text!)
        }
    }
    
}
