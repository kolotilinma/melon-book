//
//  DishData.swift
//  MelonBook
//
//  Created by Михаил on 24.09.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

struct DishData: Codable {
//    static func == (lhs: DishData, rhs: DishData) -> Bool {
//        lhs.uid == rhs.uid
//    }
//
//    var uid: String
    var name: String
    var photoName: String
    var recipes: [RecipeOld]
}
