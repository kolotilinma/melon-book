//
//  Recipe.swift
//  MelonBook
//
//  Created by Михаил on 24.09.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

struct RecipeOld: Codable {
    var name: String!
    var titleImage: String?
    var numberOfServings: Int?
    var time: Int?
    var difficult: Int?
    var section: [SectionOld]?
    var cookingStage: [StepCookingOld]?
    var folder: String?
    var teg: String?
    var comment: String?
}

struct SectionOld:  Codable {
    var name: String
    var ingredient: [IngredientOld]
}

struct IngredientOld: Codable {
    var name: String
    var amount: Int?
    var unit: String
}

struct StepCookingOld: Encodable, Decodable {
    var number: Int
    var description: String
    var image: String?
}
