//
//  User.swift
//  MelonBook
//
//  Created by Михаил on 18.10.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import Foundation
import GoogleSignIn

class User: Hashable, Decodable {
    static func == (lhs: User, rhs: User) -> Bool {
        return lhs.objectId == rhs.objectId
    }
    
    var objectId: String
    var email: String
    var firstname: String
    var lastname: String
    var fullname: String
    var avatar: String
    let loginMethod: String
    
    
    //MARK: Initializers
    init(_objectId: String, _email: String, _firstname: String, _lastname: String, _avatar: String = "", _loginMethod: String) {
        objectId = _objectId
        email = _email
        firstname = _firstname
        lastname = _lastname
        fullname = _firstname + " " + _lastname
        avatar = _avatar
        loginMethod = _loginMethod
    }
    
    init?(_dictionary: NSDictionary) {
        objectId = _dictionary[kOBJECTID] as! String
        if let mail = _dictionary[kEMAIL] { email = mail as! String } else { email = "" }
        if let fname = _dictionary[kFIRSTNAME] { firstname = fname as! String } else { firstname = "" }
        if let lname = _dictionary[kLASTNAME] { lastname = lname as! String } else { lastname = "" }
        fullname = firstname + " " + lastname
        if let avat = _dictionary[kAVATAR] { avatar = avat as! String } else { avatar = "" }
        if let lgm = _dictionary[kLOGINMETHOD] { loginMethod = lgm as! String } else { loginMethod = "" }
    }
    
    class func currentUser() -> User? {
        if let dictionary = UserDefaults.standard.object(forKey: kCURRENTUSER) {
            return User.init(_dictionary: dictionary as! NSDictionary)
        }
        return nil
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(objectId)
    }
    
    
    
    
}

//MARK: Save user funcs
func saveUserLocally(user: User) {
    UserDefaults.standard.set(userDictionaryFrom(user: user), forKey: kCURRENTUSER)
    UserDefaults.standard.synchronize()
}

func userDictionaryFrom(user: User) -> NSDictionary {
    return NSDictionary(objects: [user.objectId, user.email,
                                  user.loginMethod, user.firstname,
                                  user.lastname, user.fullname, user.avatar],
                        forKeys: [kOBJECTID as NSCopying, kEMAIL as NSCopying,
                                  kLOGINMETHOD as NSCopying, kFIRSTNAME as NSCopying,
                                  kLASTNAME as NSCopying, kFULLNAME as NSCopying,
                                  kAVATAR as NSCopying])
}
