//
//  AuthService.swift
//  MelonBook
//
//  Created by Михаил on 20.10.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import Foundation
import SwiftyVK
import FBSDKLoginKit
import GoogleSignIn

protocol AuthServiceDelegate: class {
    func authServiceShouldShow(_ viewController: UIViewController)
    func authServiceSignIn(user: User)
    func authServiceDidSignInFail(error: Error)
}

class AuthService: NSObject {
    
    private let appIdVk = "7633996"
    private let scopesVK: Scopes = [.offline,.photos,.email]
    private let fbLoginManager: LoginManager
    
    weak var delegate: AuthServiceDelegate?
    
    private struct vkUser: Decodable {
        var id: Int
        var first_name: String
        var last_name: String
        var photo_200: String
        
        static let placeholder = vkUser(id: 0, first_name: "No information", last_name: "", photo_200: "photo_200")
        static let empty = vkUser(id: 0, first_name: "", last_name: "", photo_200: "")
    }

    //MARK: - Initialize
    override init() {
        fbLoginManager = LoginManager()
        super.init()
        VK.setUp(appId: appIdVk, delegate: self)
        GIDSignIn.sharedInstance()?.delegate = self
        print("AuthService.initialize")
    }
    
    //MARK: - Google_sdk
    func loginWithGoogle(_ controller: UIViewController!) {
        GIDSignIn.sharedInstance()?.presentingViewController = controller
        GIDSignIn.sharedInstance().signIn()
//        GIDSignIn.sharedInstance()?.signOut()
    }
    
    //MARK: - Facebook_sdk
    func loginWithFacebook(_ controller: UIViewController!) {
        let fbPermission = ["email", "public_profile"]
        fbLoginManager.logIn(permissions: fbPermission, from: controller) { (result, error) in
            if let error = error {
                self.delegate?.authServiceDidSignInFail(error: error)
                return
            }
            guard let accessToken = AccessToken.current?.tokenString else {
                print("Failed to get access token")
                return
            }
            if result?.isCancelled == true {
                print("Login with Facebook is Cancelled!")
                return
            }
            let request = FBSDKLoginKit.GraphRequest(graphPath: "me",
                                                     parameters: ["fields": "email, name, picture.width(480).height(480)"],
                                                     tokenString: accessToken,
                                                     version: nil,
                                                     httpMethod: .get)
            request.start { (connection, result, error) in
                if let error = error {
                    self.delegate?.authServiceDidSignInFail(error: error)
                    return
                }
                let field = result! as? [String: Any]
                let id = field!["id"] as? String
                let fullName = field!["name"] as? String
                let email = field!["email"] as? String
                let imageURL = ((field!["picture"] as? [String: Any])?["data"] as? [String: Any])?["url"] as? String
                
                if let fullname = fullName {
                    var components = fullname.components(separatedBy: " ")
                    if (components.count > 0) {
                        let firstName = components.removeFirst()
                        let lastName = components.joined(separator: " ")
                        let user = User(_objectId: id!, _email: email ?? "", _firstname: firstName, _lastname: lastName, _avatar: imageURL ?? "", _loginMethod: kFACEBOOK)
                        self.delegate?.authServiceSignIn(user: user)
                    }
                }
            }
        }
    }
    
    //MARK: - VK_ios_sdk
    func vkWakeUpSession() {
        VK.sessions.default.logOut()
        let user = User(_objectId: "", _email: "", _firstname: "", _lastname: "", _avatar: "", _loginMethod: kVKONTAKTE)
        VK.sessions.default.logIn(
            onSuccess: { info in
                let email = (info["email"] ?? "") as String
                let user_id = (info["user_id"] ?? "") as String
                user.objectId = user_id
                user.email = email
                
                    VK.API.Users.get([.fields: "photo_200"])
                        .configure(with: Config.init(httpMethod: .POST, language: Language.default))
                        .onSuccess {
                            let userProfile = try JSONDecoder().decode([vkUser].self, from: $0)
                            guard let currentUser = userProfile.first else { return }
                            user.firstname = currentUser.first_name
                            user.lastname = currentUser.last_name
                            user.avatar = currentUser.photo_200
                            self.delegate?.authServiceSignIn(user: user)
                        }
                        .onError { (error) in
                            print("SwiftyVK: friends.get fail \n \(error.localizedDescription)")
                            self.delegate?.authServiceDidSignInFail(error: error)
                        }
                        .send()
                
            },
            onError: { error in
                print("SwiftyVK: authorize failed with", error.localizedDescription)
                self.delegate?.authServiceDidSignInFail(error: error)
            }
        )
    }
    
    func vkSdkUserAuthorizationFailed() {
        print(#function)
    }
    
    func logOut(from authMethod: String?) {
        if authMethod == kVKONTAKTE {
            VK.sessions.default.logOut()
        } else if authMethod == kFACEBOOK {
            fbLoginManager.logOut()
        } else if authMethod == kGOOGLE {
            GIDSignIn.sharedInstance()?.signOut()
        } else {
            print("Error: Cant find auth method!")
        }
        print(#function, authMethod ?? "")
    }
    
}

//MARK: - VKSdkDelegate, VKSdkUIDelegate
extension AuthService: SwiftyVKDelegate {
    func vkNeedsScopes(for sessionId: String) -> Scopes {
        return scopesVK
    }
    
    func vkNeedToPresent(viewController: VKViewController) {
        if let topViewController = UIApplication.topViewController() {
            topViewController.present(viewController, animated: true)
        }
    }
    
    func vkTokenCreated(for sessionId: String, info: [String : String]) {
        print("token created in session \(sessionId) with info \(info)")
    }
    
    func vkTokenUpdated(for sessionId: String, info: [String : String]) {
        print("token updated in session \(sessionId) with info \(info)")
    }
    
    func vkTokenRemoved(for sessionId: String) {
        print("token removed in session \(sessionId)")
    }
    
}


// MARK: - GIDSignInDelegate
extension AuthService: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("The user has not signed in before or they have since signed out.")
            } else {
                delegate?.authServiceDidSignInFail(error: error)
            }
            return
        }
        let avatarURL = user.profile.imageURL(withDimension: 600)
        let avatarString = avatarURL?.absoluteString
        let user = User(_objectId: user.userID, _email: user.profile.email, _firstname: user.profile.givenName, _lastname: user.profile.familyName, _avatar: avatarString ?? "", _loginMethod: kGOOGLE)
        delegate?.authServiceSignIn(user: user)
    }
    
}
