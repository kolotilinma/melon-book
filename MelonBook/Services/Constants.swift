//
//  Constants.swift
//  MelonBook
//
//  Created by Михаил on 18.10.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import Foundation

//User
public let kOBJECTID = "objectId"
public let kEMAIL = "email"
public let kFIRSTNAME = "firstname"
public let kLASTNAME = "lastname"
public let kFULLNAME = "fullname"
public let kAVATAR = "avatarStringURL"
public let kCURRENTUSER = "currentUser"
public let kLOGINMETHOD = "loginMethod"
public let kFACEBOOK = "facebook"
public let kGOOGLE = "google"
public let kVKONTAKTE = "vKontakte"
