//
//  CoreDataManager.swift
//  MelonBook
//
//  Created by Михаил on 05.10.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import CoreData

struct CoreDataManager {
    
    static var shared = CoreDataManager()
    
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentCloudKitContainer = {
        
        let container = NSPersistentCloudKitContainer(name: "MelonBook")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    mutating func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    mutating func fetchAllDishData() -> [DishGroup] {
        let context = persistentContainer.viewContext

        let fetchRequest: NSFetchRequest<DishGroup> = DishGroup.fetchRequest()
        do {
            let results = try context.fetch(fetchRequest)
            return results
        } catch let error as NSError {
            print(error.localizedDescription)
            return []
        }
    }
    
//    func createEmployee(employeeName: String, employeeType: String, birthday: Date, company: Company) -> (Employee?, Error?) {
//        let context = persistentContainer.viewContext
//
//        //create an employee
//        let employee = NSEntityDescription.insertNewObject(forEntityName: "Employee", into: context) as! Employee
//
//        employee.company = company
//        employee.type = employeeType
//
//        // lets check company is setup correctly
////        let company = Company(context: context)
////        company.employees
////
////        employee.company
//
//        employee.setValue(employeeName, forKey: "name")
//
//        let employeeInformation = NSEntityDescription.insertNewObject(forEntityName: "EmployeeInformation", into: context) as! EmployeeInformation
//
//        employeeInformation.taxId = "456"
//        employeeInformation.birthday = birthday
//
////        employeeInformation.setValue("456", forKey: "taxId")
//
//        employee.employeeInformation = employeeInformation
//
//        do {
//            try context.save()
//            // save succeeds
//            return (employee, nil)
//        } catch let err {
//            print("Failed to create employee:", err)
//            return (nil, err)
//        }
//
//    }
    
}
