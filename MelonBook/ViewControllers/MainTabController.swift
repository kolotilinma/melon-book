//
//  MainTabController.swift
//  MelonBook
//
//  Created by Михаил on 28.09.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

class MainTabController: UITabBarController {
    
    //MARK: - Properties
    
    
    //MARK: - Lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nil, bundle: nil)

        object_setClass(self.tabBar, CustomTabBar.self)
//        (self.tabBar as? CustomTabBar)?.setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViewControllers()

    }
    
    //MARK: - API
    
    
    //MARK: - Selectors
    
    
    
    //MARK: - Helpers
    fileprivate func configureViewControllers() {
        view.backgroundColor = .white
        let context = CoreDataManager.shared.persistentContainer.viewContext
//        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let main = MainViewController()
        main.context = context
        let newrecipe = NewRecipeViewController()
        newrecipe.backButton.isHidden = true
        newrecipe.context = context
        let search = SearchViewController()
        search.context = context
        let profile = ProfileController()

        viewControllers = [
            templateNavigationController(image: UIImage(systemName: "house"),
                                         rootViewController: main),
            templateNavigationController(image: UIImage(systemName: "plus"),
                                         rootViewController: newrecipe),
            templateNavigationController(image: UIImage(systemName: "magnifyingglass"),
                                         rootViewController: search),
            templateNavigationController(image: UIImage(systemName: "person"),
                                         rootViewController: profile)
        ]
    }
    
    fileprivate func templateNavigationController(image: UIImage?, rootViewController: UIViewController) -> UINavigationController {
        let navController = UINavigationController(rootViewController: rootViewController)
        navController.tabBarItem.image = image
        navController.navigationBar.barTintColor = .white
        return navController
    }
    
}
