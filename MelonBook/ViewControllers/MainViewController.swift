//
//  ViewController.swift
//  MelonBook
//
//  Created by Евгения Колдышева on 04.09.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit
import CoreData

class MainViewController: UIViewController{
    //MARK: - Vars
    
    var context: NSManagedObjectContext!
    var data: [DishGroup]!
        
    fileprivate let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .white
        cv.contentInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        cv.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.register(DishHeader.self,forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "DishHeader")
        cv.register(DishCell.self, forCellWithReuseIdentifier: "DishCell")
        cv.register(AddDishCell.self, forCellWithReuseIdentifier: "AddDishCell")

        return cv
    }()
    
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        data = CoreDataManager.shared.fetchAllDishData()
        
        setupCollectionView()
        let gesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPressGesture(_:)))
        collectionView.addGestureRecognizer(gesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.navigationBar.barStyle = .default
        navigationController?.navigationBar.isHidden = true
        data = CoreDataManager.shared.fetchAllDishData()
        collectionView.reloadData()
    }

    //MARK: - Actions
    @objc func handleLongPressGesture(_ gesture: UILongPressGestureRecognizer){
        switch gesture.state {
        case .began:
            guard let targetIndexPath = collectionView.indexPathForItem(at: gesture.location(in: collectionView)) else { return }
            let item = collectionView.cellForItem(at: targetIndexPath)
            if item?.reuseIdentifier == "DishCell"{
                collectionView.beginInteractiveMovementForItem(at: targetIndexPath)
            }
        case .changed:
            collectionView.updateInteractiveMovementTargetPosition(gesture.location(in: collectionView))
        case .ended:
            guard let targetIndexPath = collectionView.indexPathForItem(at: gesture.location(in: collectionView)) else { return }
            if targetIndexPath.row < data!.count {
                collectionView.endInteractiveMovement()
            } else {
                collectionView.cancelInteractiveMovement()
            }
        default:
            collectionView.cancelInteractiveMovement()
        }
    }
    
    //MARK: - Helpers
    private func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        view.addSubview(collectionView)
        collectionView.anchor(top: view.topAnchor, left: view.leftAnchor,
                              right: view.rightAnchor, bottom: view.bottomAnchor,
                              paddingTop: -60, paddingBottom: -10)
    }
    
    func showNewFolderController() {
        let modalViewController = NewFolderViewController()
        modalViewController.modalPresentationStyle = .automatic
        modalViewController.delegate = self
        present(modalViewController, animated: true, completion: nil)
    }
}

//MARK: - NewFolderDelegate
extension MainViewController: NewFolderDelegate {
    
    func updateData() {
        data = CoreDataManager.shared.fetchAllDishData()
        collectionView.reloadData()
    }
}


//MARK: - CollectionView FlowLayout
extension MainViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/2.4, height: view.frame.height/5.5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.width/1.4)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if data?.count == 0 {
            return 1
        } else {
            return data!.count + 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == data!.count  {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddDishCell", for: indexPath) as! AddDishCell
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DishCell", for: indexPath) as! DishCell
            cell.data = self.data![indexPath.row]
            return cell
        }
    }
    
    //Move to another controllers
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == data!.count  {
            showNewFolderController()
        }
        else {
            let recipesVC = RecipesViewController()
            recipesVC.context = context
            recipesVC.dishGroup = data[indexPath.row]
            self.navigationController?.pushViewController(recipesVC, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        if destinationIndexPath.item < data!.count {
            let item = data!.remove(at: sourceIndexPath.item)
            data!.insert(item, at: destinationIndexPath.item)

        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if (kind == UICollectionView.elementKindSectionHeader) {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "DishHeader", for: indexPath)
            
            return headerView
        }
        fatalError()
    }
    
}

// MARK: - SwiftUI
import SwiftUI

struct MainVCProvider: PreviewProvider {
    static var previews: some View {
        ContainerView().edgesIgnoringSafeArea(.all)
    }
    
    struct ContainerView: UIViewControllerRepresentable {
        
        let tabBarVC = MainViewController()
        
        func makeUIViewController(context: UIViewControllerRepresentableContext<MainVCProvider.ContainerView>) -> MainViewController {
            return tabBarVC
        }
        
        func updateUIViewController(_ uiViewController: MainVCProvider.ContainerView.UIViewControllerType, context: UIViewControllerRepresentableContext<MainVCProvider.ContainerView>) {
            
        }
    }
}

