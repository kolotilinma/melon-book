//
//  AddNewFolder.swift
//  MelonBook
//
//  Created by Евгения Колдышева on 07.09.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit
import CoreData

protocol NewFolderDelegate: class {
    func updateData()
}

protocol selectViewCellDelegate: NSObjectProtocol {
    func didSelectItem(image: UIImage)
}

class NewFolderViewController: UIViewController, UIImagePickerControllerDelegate, UITextViewDelegate, selectViewCellDelegate {

    //MARK: - Vars
    weak var delegate: NewFolderDelegate?
    var recipeData: DishGroup? {
        didSet {
            guard let data = recipeData else { return }
            dishImageView.label.text = "Поменять фото"
            folderNameTextView.text = data.name
            guard let imageData = data.dishImageData else { return }
            guard let image = UIImage(data: imageData) else { return }
            didSelectItem(image: image)
            dishImageView.iconImage.isHidden = false
            saveButton.isEnabled = true
            saveButton.backgroundColor = #colorLiteral(red: 0.5960784314, green: 0.5960784314, blue: 0.5960784314, alpha: 1)
            dishImageView.imageView.alpha = 0.3
        }
    }
    var tempImages = ["Avocado dish", "Soup", "Second dish", "Salad", "Add folder", "Side dish", "Snack", "Cake", "Dessert"]
    var imagePicker = UIImagePickerController()

    fileprivate let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.accessibilityScroll(.up)
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.isScrollEnabled = true
        scrollView.layer.zPosition = 1
        return scrollView
    }()
    
    fileprivate let shadowLayerView: UIView = {
        let imageView = UIView()
        imageView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4).cgColor
        imageView.layer.shadowOpacity = 0.5
        imageView.layer.shadowRadius = 4
        imageView.layer.shadowOffset = CGSize(width: -5, height: 5)
        imageView.layer.masksToBounds = false
        imageView.layer.cornerRadius = 15
        return imageView
    }()
    
    fileprivate var dishImageView = TitleRecipeImageView(title: "Загрузите фото", image: #imageLiteral(resourceName: "NewFolderBackground"))
        
    fileprivate let closeButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "Xmark"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        button.layer.zPosition = 1
        button.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        return button
    }()
        
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.isPagingEnabled = true
        collectionView.backgroundColor = #colorLiteral(red: 0.9529411765, green: 0.9490196078, blue: 0.9490196078, alpha: 1)
        collectionView.register(TemplateCell.self, forCellWithReuseIdentifier: "TemplateCell")
        return collectionView
    }()
    
    fileprivate let pageController: UIPageControl = {
        let pageControll = UIPageControl()
        pageControll.backgroundColor = UIColor.clear
        pageControll.currentPage = 0
        pageControll.pageIndicatorTintColor = #colorLiteral(red: 0.8274509804, green: 0.8235294118, blue: 0.831372549, alpha: 1)
        return pageControll
    }()
    
    fileprivate let saveButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = #colorLiteral(red: 0.8282484412, green: 0.8234216571, blue: 0.8319610953, alpha: 1)
        button.isEnabled = false
        button.setTitle("Сохранить", for: .normal)
        button.titleLabel?.font = .ahellya(15)
        button.layer.cornerRadius = 24
        button.addTarget(self, action: #selector(saveButtonTapped), for: .touchUpInside)
        return button
    }()
    
    fileprivate let folderNameTextView: UITextView = {
        let textView = UITextView()
        textView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        textView.layer.cornerRadius = 12
        textView.layer.borderWidth = 3
        textView.layer.borderColor = UIColor(red: 0.933, green: 0.933, blue: 0.933, alpha: 1).cgColor
        textView.text = "Название папки"
        textView.font = .ahellya(23)
        textView.textColor = .lightGray
        textView.textAlignment = .center
        return textView
    }()
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
//        hideKeyboardWhenTappedAround()
        setupUI()
        setupConstraint()
        folderNameTextView.delegate = self
        registerForKeyboardNotifications()
        setupGestures()
    }
    
    deinit {
        removeKeyboardNotifications()
    }
    
    //MARK: - SetupUI
    private func setupUI() {
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        scrollView.backgroundColor = #colorLiteral(red: 0.9529411765, green: 0.9490196078, blue: 0.9490196078, alpha: 1)
        scrollView.layer.cornerRadius = 12
    }
    
    private func setupConstraint() {
        view.addSubview(shadowLayerView)
        shadowLayerView.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor,
                               right: view.rightAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor,
                               paddingTop: 40, paddingLeft: 15, paddingRight: 15, paddingBottom: 40)
        shadowLayerView.addSubview(scrollView)
        view.addSubview(closeButton)
        closeButton.anchor(top: view.safeAreaLayoutGuide.topAnchor, right: view.rightAnchor,
                           paddingTop: 65, paddingRight: 35, width: 35, height: 35)
        scrollView.anchor(top: shadowLayerView.topAnchor, left: shadowLayerView.leftAnchor,
                          right: shadowLayerView.rightAnchor, bottom: shadowLayerView.bottomAnchor)
        
        scrollView.addSubview(dishImageView)
        dishImageView.anchor(left: scrollView.leftAnchor, right: scrollView.rightAnchor,
                             paddingLeft: 13, paddingRight: 13)
        dishImageView.centerX(inView: scrollView, topAnchor: scrollView.topAnchor, paddingTop: 13)
                
        let chooseTemplLable = UILabel(text: "Или воспользуйтесь шаблоном", font: .ahellya(15), color: #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1))
        scrollView.addSubview(chooseTemplLable)
        chooseTemplLable.centerX(inView: dishImageView, topAnchor: dishImageView.bottomAnchor, paddingTop: 15)
        
        scrollView.addSubview(collectionView)
        collectionView.anchor(top: chooseTemplLable.bottomAnchor, left: scrollView.leftAnchor, right: scrollView.rightAnchor,
                              paddingTop: 5, paddingLeft: 15, paddingRight: 15, height: 90)
        
        scrollView.addSubview(pageController)
        pageController.anchor(left: scrollView.leftAnchor, right: scrollView.rightAnchor)
        pageController.centerX(inView: collectionView, topAnchor: collectionView.bottomAnchor, paddingTop: -10)
        pageController.numberOfPages = tempImages.count/3
        
        scrollView.addSubview(folderNameTextView)
        folderNameTextView.anchor(top: collectionView.bottomAnchor,
                                  left: view.leftAnchor, right: view.rightAnchor,
                                  paddingTop: 25, paddingLeft: 70, paddingRight: 70, height: 70)
        
        scrollView.addSubview(saveButton)
        saveButton.anchor(top: folderNameTextView.bottomAnchor,
                          left: view.leftAnchor, right: view.rightAnchor,
                          paddingTop: 15, paddingLeft: 100, paddingRight: 100,  height: 46)
    }
    
    //MARK: - Actions
    func didSelectItem(image: UIImage) {
        self.dishImageView.imageView.image = image
        self.dishImageView.imageView.alpha = 1
        self.dishImageView.iconImage.isHidden = true
        self.dishImageView.label.isHidden = true
    }
    
    @objc func saveButtonTapped() {
        let privateContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        privateContext.parent = CoreDataManager.shared.persistentContainer.viewContext
        if recipeData != nil {
            self.recipeData!.name = folderNameTextView.text
            guard let image = dishImageView.imageView.image else { return }
            let imageData = image.pngData()
            self.recipeData!.dishImageData = imageData
        } else {
            let dishData = DishGroup(context: privateContext)
            dishData.name = folderNameTextView.text
            guard let image = dishImageView.imageView.image else { return }
            let imageData = image.pngData()
            dishData.dishImageData = imageData
        }
        do {
            try privateContext.save()
            try privateContext.parent?.save()
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        delegate?.updateData()
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func closeAction(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func imageTapped(){
        ImagePickerManager().pickImage(self){ image in
            self.dishImageView.imageView.image = image
            self.dishImageView.imageView.alpha = 1
            self.dishImageView.iconImage.isHidden = true
            self.dishImageView.label.isHidden = true
            if self.folderNameTextView.text != "" && self.folderNameTextView.text != "Название папки"{
                self.saveButton.backgroundColor = #colorLiteral(red: 0.5960784314, green: 0.5960784314, blue: 0.5960784314, alpha: 1)
            }
        }
    }
    
    @objc func backgroundTap() {
        dissmissKeyboard()
    }
    
    @objc func kbWillShow(_ notification: Notification) {
        let userInfo = notification.userInfo
        let kbFrameSize = (userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let offset = saveButton.layer.position.y - kbFrameSize.height
        scrollView.contentOffset = CGPoint(x: 0, y: offset)

    }
    
    @objc func kbWillHide() {
        scrollView.contentOffset = CGPoint.zero
    }
    
    //MARK: - Helpers
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            if recipeData == nil {
                textView.text = nil
            }
            textView.textColor = #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1)
        }
    }
    func textViewDidChange(_ textView: UITextView) {
        if textView.text != "" && textView.text != "Название папки" {
            saveButton.backgroundColor = #colorLiteral(red: 0.5960784314, green: 0.5960784314, blue: 0.5960784314, alpha: 1)
            saveButton.isEnabled = true
        } else {
            saveButton.backgroundColor = #colorLiteral(red: 0.8282484412, green: 0.8234216571, blue: 0.8319610953, alpha: 1)
            saveButton.isEnabled = false
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offSet = scrollView.contentOffset.x
        let width = scrollView.frame.width
        let horizontalCenter = width / 2

        pageController.currentPage = Int(offSet + horizontalCenter) / Int(width)
    }
    
    private func dissmissKeyboard() {
        self.view.endEditing(false)
    }

    private func setupBackgroundTouch() {
        scrollView.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(backgroundTap))
        scrollView.addGestureRecognizer(tapGesture)
    }
    
    private func setupGestures() {
        setupBackgroundTouch()
        let gesture = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        dishImageView.isUserInteractionEnabled = true
        dishImageView.addGestureRecognizer(gesture)
        let gesBackgroundTap = UITapGestureRecognizer(target: self, action: #selector(closeAction))
        view.addGestureRecognizer(gesBackgroundTap)
    }
    
    //MARK: - Notifications
    func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(kbWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(kbWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func removeKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
}

//MARK: - CollectionView DataSource
extension NewFolderViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate {
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/3.3, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tempImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TemplateCell", for: indexPath) as! TemplateCell
        cell.delegate = self
        cell.layer.shadowColor = .init(srgbRed: 0, green: 0, blue: 0, alpha: 0.07)
        cell.layer.shadowRadius = 4
        cell.layer.shadowOpacity = 1
        cell.layer.shadowOffset = CGSize(width: 0, height: 4)
        cell.imageString = self.tempImages[indexPath.row]
        
        return cell
    }
}

// MARK: - SwiftUI
import SwiftUI

struct NewFolderVCProvider: PreviewProvider {
    static var previews: some View {
        ContainerView().edgesIgnoringSafeArea(.all)
    }
    
    struct ContainerView: UIViewControllerRepresentable {
        
        let tabBarVC = NewFolderViewController()
        
        func makeUIViewController(context: UIViewControllerRepresentableContext<NewFolderVCProvider.ContainerView>) -> NewFolderViewController {
            return tabBarVC
        }
        
        func updateUIViewController(_ uiViewController: NewFolderVCProvider.ContainerView.UIViewControllerType, context: UIViewControllerRepresentableContext<NewFolderVCProvider.ContainerView>) {
            
        }
    }
}
