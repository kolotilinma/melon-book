//
//  NewRecipe.swift
//  MelonBook
//
//  Created by Евгения Колдышева on 16.09.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift

class NewRecipeViewController: UIViewController, UIGestureRecognizerDelegate {
    
    //MARK: - Vars
    
    var context: NSManagedObjectContext!
    var newRecipe: Recipe?
    var underKeyboardHeight: CGFloat = 0

    fileprivate var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.backgroundColor = #colorLiteral(red: 0.933, green: 0.933, blue: 0.933, alpha: 1.0)
        scrollView.isScrollEnabled = true
        return scrollView
    }()
    
    let backButton = UIButton(image: UIImage(systemName: "arrow.left")!)
    fileprivate var dishImageView = TitleRecipeImageView(title: "Загрузите фото", image: #imageLiteral(resourceName: "Rectangle 83"))
    fileprivate var recipeNameTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Название рецепта:"
        textField.backgroundColor = .white
        textField.font = .ahellya(15)
        textField.textColor = UIColor(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1)
        textField.layer.cornerRadius = 12
        textField.contentVerticalAlignment = .center
        textField.textAlignment = .justified
        textField.setLeftPaddingPoints(10)
        return textField
    }()
    
    fileprivate let numberOfServicesView = NewRecipeTitleElemView(text: "Количество порций:", image: #imageLiteral(resourceName: "Rectangle 91"),
                                                                  placeholder: "1", leftPadding: 55)
    fileprivate let timeView = NewRecipeTitleTimeView(text: "Время приготовления:", image: #imageLiteral(resourceName: "Rectangle 92"),
                                                      placeholder: "15 минут")
    fileprivate let difficultyView = NewRecipeDifficultyView(text: "Сложность:", image: #imageLiteral(resourceName: "Rectangle 93"),
                                                            placeholder: "2 - средняя", leftPadding: 25)
    fileprivate let ingredientsLabel = UILabel(text: "Ингредиенты:", font: .ahellya(23), color: #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1))
    fileprivate let ingredientTableView = NewRecipeIngredientsView()
    fileprivate let stepTableView = NewRecipeStepView()
    let folderView = NewRecipeFoldersTableView()
    fileprivate let tagsView = NewRecipeTagsView()
    fileprivate let commentView = NewRecipeCommentView()
    
    fileprivate let saveButtonView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    fileprivate let saveButton: UIButton = {
        let saveB = UIButton()
        saveB.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        saveB.setTitle("Сохранить рецепт", for: .normal)
        saveB.titleLabel?.font = .ahellya(15)
        saveB.layer.cornerRadius = 24
        saveB.addTarget(self, action: #selector(saveButtonTapped), for: .touchUpInside)
        return saveB
    }()
        
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
//        hideKeyboardWhenTappedAround()
        
        setupIQKeyboardManager()
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        setupController()
        setupTextFields()
        setupPickerViews()
        folderView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.navigationBar.barStyle = .default
        navigationController?.navigationBar.isHidden = true
        setupNewRecipe()
        
    }
    
    private func setupNewRecipe() {
        if newRecipe == nil {
            newRecipe = Recipe(context: context)
            folderView.setupFirstFolder(with: nil)
        }
    }
    
    
    //MARK: - SetupUI
    private func setupController() {
        view.backgroundColor = UIColor(red: 0.933, green: 0.933, blue: 0.933, alpha: 1)
        view.addSubview(scrollView)
        scrollView.addConstraintsToFillView(view)
        
        let underKeyboardView = UIView()
        let contentView = UIStackView(
            arrangedSubviews: [dishImageView, recipeNameTextField, numberOfServicesView, timeView,
                               difficultyView, ingredientTableView, stepTableView, folderView,
                               tagsView, commentView, saveButton, underKeyboardView],
            axis: .vertical, spacing: 26)
        contentView.backgroundColor = .clear
        
        scrollView.addSubview(contentView)
        contentView.addConstraintsToFillView(scrollView)
        dishImageView.anchor(left: view.leftAnchor, right: view.rightAnchor,
                             paddingLeft: 13, paddingRight: 13)
        recipeNameTextField.anchor(left: view.leftAnchor, right: view.rightAnchor,
                                   paddingLeft: 25, paddingRight: 25, height: 63)
        numberOfServicesView.anchor(left: view.leftAnchor, right: view.rightAnchor,
                                    paddingLeft: 25, paddingRight: 25, height: 46)
        timeView.anchor(left: view.leftAnchor, right: view.rightAnchor,
                        paddingLeft: 25, paddingRight: 25, height: 46)
        difficultyView.anchor(left: view.leftAnchor, right: view.rightAnchor,
                              paddingLeft: 25, paddingRight: 25, height: 46)
        ingredientTableView.anchor(left: view.leftAnchor, right: view.rightAnchor)
//                                   paddingLeft: 25, paddingRight: 25)
        stepTableView.anchor(left: view.leftAnchor, right: view.rightAnchor,
                             paddingLeft: 25, paddingRight: 25)
        folderView.anchor(left: view.leftAnchor, right: view.rightAnchor,
                          paddingLeft: 25, paddingRight: 25, height: 200)
        tagsView.anchor(left: view.leftAnchor, right: view.rightAnchor,
                           paddingLeft: 25, paddingRight: 25, height: 120)
        commentView.anchor(left: view.leftAnchor, right: view.rightAnchor,
                           paddingLeft: 25, paddingRight: 25, height: 92)
        saveButton.anchor(left: view.leftAnchor, right: view.rightAnchor,
                          paddingLeft: 50, paddingRight: 50, height: 45)
        underKeyboardView.anchor(height: 50)
                
        view.addSubview(backButton)
        backButton.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor,
                          paddingTop: 3, paddingLeft: 7, width: 45, height: 45)
    }
    
    private func setupTextFields() {
        recipeNameTextField.delegate = self
        numberOfServicesView.servTextField.delegate = self
        timeView.textField.delegate = self
        difficultyView.difficultyTextField.delegate = self
        CreateIngredientCell().unitTextField.delegate = self
    }
    
    private func setupPickerViews() {
//        numOfServDropDown.delegate = self as UIPickerViewDelegate
//        numOfServDropDown.dataSource = self as UIPickerViewDataSource
//        difficultyDropDown.delegate = self as UIPickerViewDelegate
//        difficultyDropDown.dataSource = self as UIPickerViewDataSource
//        unitsDropDown.delegate = self as UIPickerViewDelegate
//        unitsDropDown.dataSource = self as UIPickerViewDataSource
    }
    
    //MARK: - Actions
    @objc func backAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
//    @objc func backgroundTap() {
//        dissmissKeyboard()
//        numOfServDropDown.isHidden = true
//        difficultyDropDown.isHidden = true
//        unitsDropDown.isHidden = true
//    }
    
//    private func dissmissKeyboard() {
//        self.view.endEditing(false)
//    }
        
    @objc func saveButtonTapped(){
        if recipeNameTextField.text != nil && recipeNameTextField.text != "" {
            saveRecipe { (error) in
                if let error = error {
                    self.showAlert(with: "Ошибка", and: error.localizedDescription)
                    print(error.localizedDescription)
                }
                self.cleanAllFields()
                let controller = RecipeViewController()
                controller.recipe = self.newRecipe
                self.navigationController?.pushViewController(controller, animated: true)
            }
            recipeNameTextField.layer.borderWidth = 0
            recipeNameTextField.layer.borderColor = .none
        } else {
            recipeNameTextField.layer.borderWidth = 2
            recipeNameTextField.layer.borderColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1)
            showAlert(with: "Ошибка", and: "Заполните обязательные поля")
            scrollView.contentOffset = CGPoint.zero
        }
    }
        
    //MARK: - Helpers
    
    private func cleanAllFields() {
        self.recipeNameTextField.text = ""
        self.dishImageView.imageView.image = #imageLiteral(resourceName: "Rectangle 83")
        self.dishImageView.imageView.alpha = 0.3
        self.dishImageView.iconImage.isHidden = false
        self.dishImageView.label.text = "Загрузите фото"
        self.tagsView.label.text = ""
        self.commentView.textView.text = ""
        self.difficultyView.difficultyTextField.text = ""
        self.numberOfServicesView.servTextField.text = ""
        self.timeView.textField.text = ""
        self.stepTableView.steps = [["":#imageLiteral(resourceName: "Camera")]]
        self.stepTableView.tableView.reloadData()
    }
    
    //MARK: saveRecipe
    private func saveRecipe(completion: @escaping (Error?) -> Void) {
        newRecipe?.name = recipeNameTextField.text

        if difficultyView.difficultyTextField.text != nil {
            let difficult = (difficultyView.difficultyTextField.text! as NSString).integerValue
            newRecipe?.difficult = Int16(difficult)
        }
        if numberOfServicesView.servTextField.text != nil {
            let numOfServ = (numberOfServicesView.servTextField.text! as NSString).integerValue
            newRecipe?.numberOfServings = Int16(numOfServ)
        }
        if timeView.textField.text != nil {
            let time = (timeView.textField.text! as NSString).integerValue
            newRecipe?.time = Int16(time)
        }
        newRecipe?.comment = commentView.textView.text
        newRecipe?.teg = tagsView.label.text
    
        guard let image = dishImageView.imageView.image else { return }
        let imageData = image.pngData()
        newRecipe?.recipeImageData = imageData
        
        stepTableView.tableView.visibleCells.forEach { (cell) in
            if let cell = cell as? StepCell {
                let cookingStage = StepCooking(context: context)
                cookingStage.recipe = newRecipe
                
                let stepNumberString = cell.stepLabel.text
                guard let stepNumberLast = stepNumberString?.suffix(1) else { return }
                let stepNumber = (stepNumberLast as NSString).integerValue
                cookingStage.number = Int16(stepNumber)
                
                cookingStage.stepDescription = cell.stepTextView.text
                
                let imageStepCell = cell.cameraImageView.image
                let imageDataStepCell = imageStepCell!.pngData()
                cookingStage.stepImageData = imageDataStepCell
            }
        }
        ingredientTableView.ingridients.forEach { (sectionRecipe) in
            let section = Section(context: context)
            section.recipe = newRecipe
            section.name = sectionRecipe.name
            sectionRecipe.ingredient.forEach { (ingredientRecipe) in
                let ingredient = Ingredient(context: context)
                ingredient.section = section
                ingredient.name = ingredientRecipe.name
                if let amount = ingredientRecipe.amount {
                    ingredient.amount = Int16(amount)
                }
                ingredient.unit = ingredientRecipe.unit
            }
        }
        do {
            try context.save()
            completion(nil)
        } catch let error as NSError {
            completion(error)
        }
    }
    
    private func setupIQKeyboardManager() {
        IQKeyboardManager.shared.enable = true
        let keyboardHideImage = UIImage(systemName: "keyboard.chevron.compact.down")!
            .withTintColor(.darkGray, renderingMode: .alwaysTemplate)
        IQKeyboardManager.shared.toolbarDoneBarButtonItemImage = keyboardHideImage
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.shouldPlayInputClicks = false
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 60
        IQKeyboardManager.shared.toolbarTintColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
    }
    
}

// MARK: - TextFieldDelegate
extension NewRecipeViewController: UITextFieldDelegate {
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if textField == recipeNameTextField {
            if textField.text != "" {
                recipeNameTextField.layer.borderWidth = 0
                recipeNameTextField.layer.borderColor = .none
            }
        }
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == timeView.textField {
            if textField.text != "" {
                textField.text?.append(" минут")
            } else {
                textField.placeholder = "15 минут"
            }
//            dissmissKeyboard()
        }
    }
    
}

extension NewRecipeViewController: NewRecipeFoldersTableViewDelegate {
    func selectedFolder(folder: DishGroup) {
        newRecipe?.dishGroup = folder
    }
}

// MARK: - SwiftUI
import SwiftUI

struct NewRecipeVCProvider: PreviewProvider {
    static var previews: some View {
        ContainerView().edgesIgnoringSafeArea(.all)
    }

    struct ContainerView: UIViewControllerRepresentable {

        let tabBarVC = NewRecipeViewController()

        func makeUIViewController(context: UIViewControllerRepresentableContext<NewRecipeVCProvider.ContainerView>) -> NewRecipeViewController {
            return tabBarVC
        }

        func updateUIViewController(_ uiViewController: NewRecipeVCProvider.ContainerView.UIViewControllerType, context: UIViewControllerRepresentableContext<NewRecipeVCProvider.ContainerView>) {

        }
    }
}

