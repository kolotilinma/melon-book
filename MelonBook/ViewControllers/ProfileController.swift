//
//  ProfileController.swift
//  MelonBook
//
//  Created by Михаил on 29.09.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit
import SDWebImage
import MessageUI

class ProfileController: UIViewController {
    
    //MARK: - Properties
    let user = User.currentUser()
    
    fileprivate let avatarImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "fPlaceholder")
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 150 / 2
        imageView.clipsToBounds = true
        return imageView
    }()
    fileprivate let userNameLabel = UILabel(text: "",font: .ahellya(34), color: #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1))
    
    fileprivate let exitButton: UIButton = {
        let button  = UIButton(type: .system)
        button.setTitle("Выход", for: .normal)
        button.setTitleColor( #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        button.backgroundColor = #colorLiteral(red: 0.5960784314, green: 0.5960784314, blue: 0.5960784314, alpha: 1)
        button.layer.cornerRadius = 25
        button.titleLabel?.font = .ahellya(15)
        button.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4).cgColor
        button.layer.shadowOpacity = 0.2
        button.layer.shadowRadius = 1.5
        button.layer.shadowOffset = CGSize(width: -3, height: 3)
        button.addTarget(self, action: #selector(handleExit), for: .touchUpInside)
        return button
    }()
    
    fileprivate let supportButton: UIButton = {
        let button  = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "support").withRenderingMode(.alwaysTemplate), for: .normal)
        button.setTitleColor( #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1), for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        button.layer.cornerRadius = 20
        button.tintColor = #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1)
        button.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4).cgColor
        button.layer.shadowOpacity = 0.2
        button.layer.shadowRadius = 1.5
        button.layer.shadowOffset = CGSize(width: -3, height: 3)
        button.addTarget(self, action: #selector(handleSupport), for: .touchUpInside)
        return button
    }()
    let loginMethodLable = UILabel(text: "Вход выполнен:", font: .ahellya(18), color: #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1))
    let emailLable = UILabel(text: "", font: .ahellya(15), color: #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1))
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        setupUserUI()
    }
    
    //MARK: - Selectors
    @objc func handleExit(){
        let optionMenu = UIAlertController(title: "Выйти из Акаунта",
                                           message: "Вы уверены, что хотите выйти?",
                                           preferredStyle: .actionSheet)
        let deleteAction = UIAlertAction(title: "Выйти", style: .destructive) { (alert) in
            // delete user action
            //TODO: Add delite database function
            self.exitUser()
        }
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel) { (alert) in
            // cancel user action
        }
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
        
    }
    
    @objc func handleSupport(){
        sendEmail()
    }
    
    //MARK: - Helpers
    fileprivate func setupUserUI() {
        userNameLabel.text = user?.fullname
        emailLable.text = user?.email
        avatarImageView.sd_setImage(with: URL(string: user?.avatar ?? ""))
        addLoginMethodImage()
    }
    
    private func addLoginMethodImage() {
        var image = UIImage()
        if user?.loginMethod == kGOOGLE {
            image = #imageLiteral(resourceName: "googleLogo")
        } else if user?.loginMethod == kFACEBOOK {
            image = #imageLiteral(resourceName: "facebookLogo")
        } else if user?.loginMethod == kVKONTAKTE {
            image = #imageLiteral(resourceName: "vkLogo")
        }
        let logoImageView = UIImageView(image: image, contentMode: .scaleAspectFit)
        view.addSubview(logoImageView)
        logoImageView.anchor(width: 30, height: 30)
        logoImageView.centerY(inView: loginMethodLable, leftAnchor: loginMethodLable.rightAnchor, paddingLeft: 4)
    }
    
    func exitUser() {
        let authService = SceneDelegate.shared().authService
        authService?.logOut(from: user?.loginMethod)
        // delete user locally
        UserDefaults.standard.removeObject(forKey: kCURRENTUSER)
        UserDefaults.standard.synchronize()
        goToStartPage()
    }
    
    func goToStartPage() {
        let controller = StartPageViewController()
        controller.modalPresentationStyle = .fullScreen
        present(controller, animated: true, completion: nil)
    }
    
    fileprivate func configureUI() {
        view.backgroundColor = .white
        navigationController?.navigationBar.isHidden = true
        view.addSubview(avatarImageView)
        avatarImageView.centerX(inView: view, topAnchor: view.safeAreaLayoutGuide.topAnchor, paddingTop: 40)
        avatarImageView.setDimensions(width: 150, height: 150)

        view.addSubview(userNameLabel)
        userNameLabel.centerX(inView: view, topAnchor: avatarImageView.bottomAnchor, paddingTop: 30)
        
        view.addSubview(loginMethodLable)
        loginMethodLable.centerX(inView: userNameLabel, topAnchor: userNameLabel.bottomAnchor,
                                 paddingTop: 50, offset: -15)
        loginMethodLable.textAlignment = .center
        
        view.addSubview(emailLable)
        emailLable.centerX(inView: userNameLabel, topAnchor: loginMethodLable.bottomAnchor, paddingTop: 20)
                
        let chooseTemplLable = UILabel(text: "Связаться с поддержкой", font: .ahellya(15), color: #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1))
        view.addSubview(chooseTemplLable)
        chooseTemplLable.anchor(left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor,
                                paddingLeft: view.frame.size.width / 2 - 50, paddingBottom: 50)
        view.addSubview(supportButton)
        supportButton.centerY(inView: chooseTemplLable)
        supportButton.anchor(right: chooseTemplLable.leftAnchor,
                             paddingRight: 10, width: 60, height: 40)
        view.addSubview(exitButton)
        exitButton.centerX(inView: view)
        exitButton.anchor(left: view.leftAnchor, right: view.rightAnchor,
                          bottom: chooseTemplLable.topAnchor,
                          paddingLeft: 72, paddingRight: 72, paddingBottom: 40, height: 50)
    }
    
    
}

//MARK: - MailComposeViewControllerDelegate
extension ProfileController: MFMailComposeViewControllerDelegate {
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["melonbook.info@gmail.com"])
            mail.setSubject("Заголовок")
            mail.setMessageBody("<p>Напишите тут свое сообщение!</p>", isHTML: true)

            present(mail, animated: true)
        } else {
            showAlert(with: "Ошибка", and: "Ошибка при отправке письма")
        }
    }

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}

