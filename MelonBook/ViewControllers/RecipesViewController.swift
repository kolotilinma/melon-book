//
//  RecipesViewController.swift
//  MelonBook
//
//  Created by Евгения Колдышева on 07.09.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class RecipesViewController: UIViewController {
    //MARK: - Vars
    var titleVC: String? = ""
    var dishGroup: DishGroup! {
        didSet {
            if dishGroup != nil {
                titleVC = dishGroup.name
                guard let recipes = dishGroup.recipes?.allObjects as? [Recipe] else { return }
                allRecipes = recipes
            }
        }
    }
    var allRecipes: [Recipe]?
    var context: NSManagedObjectContext!
        
    fileprivate let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .white
        cv.contentInset = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12)
        cv.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        cv.isScrollEnabled = true
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.register(RecipeHeader.self,forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "RecipeHeader")
        cv.register(RecipeCell.self, forCellWithReuseIdentifier: RecipeCell.reuseId)
        cv.register(AddRecipeCell.self, forCellWithReuseIdentifier: "AddRecipeCell")
        return cv
    }()
    
    fileprivate let backButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(UIImage(systemName: "arrow.left")!.withRenderingMode(.alwaysTemplate), for: .normal)
        btn.tintColor = #colorLiteral(red: 0.7371357679, green: 0.732755363, blue: 0.7405038476, alpha: 1)
        btn.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        return btn
    }()
    
    fileprivate let editButton: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "Rectangle 67"), for: .normal)
        btn.imageEdgeInsets = UIEdgeInsets(top: 12, left: 12, bottom: 12, right: 5)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(editAction), for: .touchUpInside)
        return btn
    }()
    
    fileprivate let removeButton: UIButton = {
        let btn = UIButton()
        let btnImg = UIImage(systemName: "trash")
        let tintedImg = btnImg?.withRenderingMode(.alwaysTemplate)
        btn.setImage(tintedImg, for: .normal)
        btn.tintColor = #colorLiteral(red: 0.7371357679, green: 0.732755363, blue: 0.7405038476, alpha: 1)
        btn.imageEdgeInsets = UIEdgeInsets(top: 3, left: 0, bottom: 9, right: 10)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(removeAction), for: .touchUpInside)
        return btn
    }()
     
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()

        collectionView.delegate = self
        collectionView.dataSource = self
        
        let gesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPressGesture(_:)))
        collectionView.addGestureRecognizer(gesture)
    }
    
    //MARK: - SetupUI
    func setupUI(){
        view.addSubview(collectionView)
        collectionView.topAnchor.constraint(equalTo: view.topAnchor, constant: -60).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -10).isActive = true
        
        view.addSubview(backButton)
        backButton.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor,
                          paddingTop: 3, paddingLeft: 7, width: 45, height: 45)
        
        view.addSubview(removeButton)
        removeButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 60).isActive = true
        removeButton.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        removeButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        removeButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        view.addSubview(editButton)
        editButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 60).isActive = true
        editButton.trailingAnchor.constraint(equalTo: removeButton.leadingAnchor).isActive = true
        editButton.heightAnchor.constraint(equalToConstant: 45).isActive = true
        editButton.widthAnchor.constraint(equalToConstant: 39).isActive = true
    }
    
    //MARK: - Actions
    @objc func handleLongPressGesture(_ gesture: UILongPressGestureRecognizer){
        switch gesture.state {
        case .began:
            guard let targetIndexPath = collectionView.indexPathForItem(at: gesture.location(in: collectionView)) else { return }
            let item = collectionView.cellForItem(at: targetIndexPath)
            if item?.reuseIdentifier == RecipeCell.reuseId {
                collectionView.beginInteractiveMovementForItem(at: targetIndexPath)

            }
        case .changed:
            collectionView.updateInteractiveMovementTargetPosition(gesture.location(in: collectionView))
        case .ended:
            guard let targetIndexPath = collectionView.indexPathForItem(at: gesture.location(in: collectionView)) else { return }
            if targetIndexPath.row != (dishGroup.recipes?.count ?? 1) - 1 {
                collectionView.endInteractiveMovement()
            } else {
                collectionView.cancelInteractiveMovement()
            }
        default:
            collectionView.cancelInteractiveMovement()
        }
    }
    
    @objc func backAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func editAction(){
        let modalViewController = NewFolderViewController()
        modalViewController.delegate = self
        modalViewController.recipeData = dishGroup
        modalViewController.modalPresentationStyle = .automatic
        present(modalViewController, animated: true, completion: nil)
    }
    
    @objc func removeAction(){
        let alert = UIAlertController(title: "Вы действительно хотите удалить папку?", message: "", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Да", style: .default, handler: { action in
            guard let dishGroup = self.dishGroup else { return }
            
            self.context.delete(dishGroup)
            
            do {
                try self.context.save()
                self.navigationController?.popViewController(animated: true)
            } catch let error as NSError {
                print(error.localizedDescription)
            }
                        
        }))
        alert.addAction(UIAlertAction(title: "Нет", style: .cancel, handler: nil))

        self.present(alert, animated: true)
    }
}

//MARK: - UICollectionViewDataSource
extension RecipesViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/2.3, height: view.frame.height/5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.width/1.9)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (allRecipes?.count ?? 0) + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == allRecipes?.count {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddRecipeCell", for: indexPath) as! AddRecipeCell            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: RecipeCell.reuseId, for: indexPath) as! RecipeCell
            cell.configure(with: allRecipes?[indexPath.item])
            return cell
        }
    }
    
    //Move to another controllers
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == allRecipes?.count  {
            let newRecipeVC = NewRecipeViewController()
            newRecipeVC.context = context
            newRecipeVC.newRecipe = Recipe(context: context)
            newRecipeVC.folderView.setupFirstFolder(with: dishGroup)
            self.navigationController?.pushViewController(newRecipeVC, animated: true)
        }
        else {
            let vcRec = RecipeViewController()
            vcRec.recipe = allRecipes?[indexPath.row]
            self.navigationController?.pushViewController(vcRec, animated: true)
        }
    }
    
    //Re-order
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        //TODO: сделать перенос по индексу
        
//        let item = dishGroup.recipes.remove(at: sourceIndexPath.row)
//        dishGroup.recipes.insert(item, at: destinationIndexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if (kind == UICollectionView.elementKindSectionHeader) {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "RecipeHeader", for: indexPath) as! RecipeHeader
            headerView.titleHeader = titleVC!
            return headerView
        }
        fatalError()
    }
    
}
//MARK: - NewFolderDelegate
extension RecipesViewController: NewFolderDelegate {
   
    func updateData() {
        titleVC = dishGroup.name
        collectionView.reloadData()
    }
}

// MARK: - SwiftUI
import SwiftUI

struct RecipeVCProvider: PreviewProvider {
    static var previews: some View {
        ContainerView().edgesIgnoringSafeArea(.all)
    }

    struct ContainerView: UIViewControllerRepresentable {

        let tabBarVC = RecipesViewController()

        func makeUIViewController(context: UIViewControllerRepresentableContext<RecipeVCProvider.ContainerView>) -> RecipesViewController {
            return tabBarVC
        }

        func updateUIViewController(_ uiViewController: RecipeVCProvider.ContainerView.UIViewControllerType, context: UIViewControllerRepresentableContext<RecipeVCProvider.ContainerView>) {

        }
    }
}

