//
//  RecipeViewController.swift
//  MelonBook
//
//  Created by Михаил on 24.09.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import Foundation
import UIKit

private let recipeReuseIdentifier = "RecipeTitleCell"
private let cookingStageReuseIdentifier = "CookingStageTableViewCell"
private let recipeCellReuseIdentifier = "RecipeTableViewCell"
private let footerRecipeCellReuseIdentifier = "FooterRecipeTableViewCell"
private let titleIngridientCellReuseIdentifier = "TitleIngridientCell"

class RecipeViewController: UIViewController {
    
    //MARK: - Properties
    var recipe: Recipe! {
        didSet {
            guard let allSections = recipe.sections?.allObjects as? [Section] else { return }
            sections = allSections
            guard let allCookingStages = recipe.cookingStage?.allObjects as? [StepCooking] else { return }
            cookingStages = allCookingStages
        }
    }
    var sections: [Section]?
    var cookingStages: [StepCooking]?
    let tableView = UITableView()
    
    fileprivate let tabBarView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private lazy var backButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(systemName: "arrow.left")!.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = #colorLiteral(red: 0.7371357679, green: 0.732755363, blue: 0.7405038476, alpha: 1)
        button.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var editButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "Rectangle 67").withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = #colorLiteral(red: 0.7371357679, green: 0.732755363, blue: 0.7405038476, alpha: 1)
        button.imageEdgeInsets = UIEdgeInsets(top: 12, left: 12, bottom: 12, right: 12)
        button.addTarget(self, action: #selector(editAction), for: .touchUpInside)
        return button
    }()
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupTableView()
    }
    
    //MARK: - SetupUI
    private func setupUI() {
        view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        view.addSubview(tableView)
        tableView.addConstraintsToFillView(view)
        
        tableView.addSubview(backButton)
        backButton.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor,
                          paddingTop: 3, paddingLeft: 7, width: 45, height: 45)
        
        view.addSubview(editButton)
        editButton.anchor(top: view.safeAreaLayoutGuide.topAnchor, right: view.rightAnchor,
                          paddingTop: 3, paddingRight: 7 , width: 45, height: 45)
    }
    
    private func setupTableView() {
        tableView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(RecipeTitleCell.self, forCellReuseIdentifier: recipeReuseIdentifier)
        tableView.register(RecipeTableViewCell.self, forCellReuseIdentifier: recipeCellReuseIdentifier)
        tableView.register(CookingStageTableViewCell.self, forCellReuseIdentifier: cookingStageReuseIdentifier)
        tableView.register(FooterRecipeTableViewCell.self, forCellReuseIdentifier: footerRecipeCellReuseIdentifier)
        tableView.register(TitleIngridientCell.self, forCellReuseIdentifier: titleIngridientCellReuseIdentifier)
        tableView.sectionHeaderHeight = 0
        tableView.sectionFooterHeight = 0
        tableView.separatorStyle = .none
    }
    
    //MARK: - Actions
    @objc func backAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func editAction(){
        //TODO: Add editAction function
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: - UITableView Delegate DataSource
extension RecipeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            guard let numberOfRecepeSection = sections?.count else { return 0 }
            return numberOfRecepeSection + 1
        } else if section == 2 {
            guard let numberOfCookingStage = cookingStages?.count else { return 0 }
            return numberOfCookingStage + 1
        } else if section == 3 {
            return 2
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 500
        } else if indexPath.section == 1 && indexPath.row == 0 {
            return 50
        } else if indexPath.section == 1 && indexPath.row > 0 {
            return CGFloat(50 + ((sections?[indexPath.row - 1].ingredients?.count ?? 0 ) * 44))
        } else if indexPath.section == 2 && indexPath.row == 0 {
            return 50
        } else if indexPath.section == 2 && indexPath.row > 0 {
            return 350
        } else if indexPath.section == 3 && indexPath.row == 0 {
            return 50
        } else {
            return 150
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: recipeReuseIdentifier, for: indexPath) as! RecipeTitleCell
            cell.recipe = recipe
            cell.selectionStyle = .none
            return cell
        } else if indexPath.section == 1 && indexPath.row == 0 {
            let titleCell = tableView.dequeueReusableCell(withIdentifier: titleIngridientCellReuseIdentifier, for: indexPath) as! TitleIngridientCell
            titleCell.selectionStyle = .none
            titleCell.titleName = "Ингредиенты:"
            return titleCell
        } else if indexPath.section == 1 && indexPath.row > 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: recipeCellReuseIdentifier, for: indexPath) as! RecipeTableViewCell
            if recipe.sections != nil {
                cell.sectionCooking = sections?[indexPath.row - 1]
            }
            cell.selectionStyle = .none
            return cell
        } else if indexPath.section == 2 && indexPath.row == 0 {
            let titleCell = tableView.dequeueReusableCell(withIdentifier: titleIngridientCellReuseIdentifier, for: indexPath) as! TitleIngridientCell
            titleCell.selectionStyle = .none
            titleCell.titleName = "Приготовление:"
            return titleCell
        } else if indexPath.section == 2 && indexPath.row > 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: cookingStageReuseIdentifier, for: indexPath) as! CookingStageTableViewCell
            if recipe.cookingStage != nil {
                cell.step = cookingStages?[indexPath.row - 1]
            }
            cell.selectionStyle = .none
            return cell
        } else if indexPath.section == 3 && indexPath.row == 0 {
            let titleCell = tableView.dequeueReusableCell(withIdentifier: titleIngridientCellReuseIdentifier, for: indexPath) as! TitleIngridientCell
            titleCell.selectionStyle = .none
            titleCell.titleName = "Комментарии"
            return titleCell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: footerRecipeCellReuseIdentifier, for: indexPath) as! FooterRecipeTableViewCell
            cell.recipe = recipe
            cell.selectionStyle = .none
            return cell
        }
    }
    
}
