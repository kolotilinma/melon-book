//
//  RegistrationViewController.swift
//  MelonBook
//
//  Created by Михаил on 17.10.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

class RegistrationViewController: UIViewController {
    
    
    //MARK: - Properties
    fileprivate let logoImageView = UIImageView(image: #imageLiteral(resourceName: "registerBG"), contentMode: .scaleAspectFit)
    let facebookButton = UIButton(title: "Войти с помощью Facebook")
    let vkontakteButton = UIButton(title: "Войти с помощью VKontakte")
    let appleButton = UIButton(title: "Войти с помощью Apple ID")
    let googleButton = UIButton(title: "Войти с помощью Google")
    private var authService: AuthService!
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        authService = SceneDelegate.shared().authService
        authService.delegate = self
        configureUI()
        setupButtonTarget()
        
    }
    
    //MARK: - Selectors
    @objc func handleFacebook() {
        authService.loginWithFacebook(self)
    }
    
    @objc func handleVkontakte() {
        authService.vkWakeUpSession()
    }
    
    @objc func handleApple() {
        print(#function)
        showAlert(with: "Ошибка :(", and: "Попробуйте Войти с помощью других соц. сетей")
    }
    
    @objc func handleGoogle() {
        authService.loginWithGoogle(self)
    }
    
    //MARK: - Helpers
    fileprivate func setupButtonTarget() {
        facebookButton.addTarget(self, action: #selector(handleFacebook), for: .touchUpInside)
        vkontakteButton.addTarget(self, action: #selector(handleVkontakte), for: .touchUpInside)
        appleButton.addTarget(self, action: #selector(handleApple), for: .touchUpInside)
        googleButton.addTarget(self, action: #selector(handleGoogle), for: .touchUpInside)
    }
    
    fileprivate func configureUI() {
        view.backgroundColor = .white
        navigationController?.navigationBar.isHidden = true
        view.addSubview(logoImageView)
        logoImageView.addConstraintsToFillView(view)
        let titleLabel = UILabel(text: "Твои лучшие рецепты в одном приложении",
                                 font: .ahellya(28), color: #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1))
        view.addSubview(titleLabel)
        titleLabel.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                          left: view.leftAnchor, right: view.rightAnchor,
                          paddingTop: 60, paddingLeft: 50, paddingRight: 50)
        titleLabel.numberOfLines = 2
        titleLabel.textAlignment = .center
        view.addSubview(facebookButton)
        facebookButton.anchor(top: titleLabel.bottomAnchor, left: view.leftAnchor, right: view.rightAnchor, paddingTop: 50, paddingLeft: 50, paddingRight: 50, height: 40)
        view.addSubview(vkontakteButton)
        vkontakteButton.anchor(top: facebookButton.bottomAnchor, left: view.leftAnchor, right: view.rightAnchor, paddingTop: 25, paddingLeft: 50, paddingRight: 50, height: 40)
        view.addSubview(appleButton)
        appleButton.anchor(top: vkontakteButton.bottomAnchor, left: view.leftAnchor, right: view.rightAnchor, paddingTop: 25, paddingLeft: 50, paddingRight: 50, height: 40)
        view.addSubview(googleButton)
        googleButton.anchor(top: appleButton.bottomAnchor, left: view.leftAnchor, right: view.rightAnchor, paddingTop: 25, paddingLeft: 50, paddingRight: 50, height: 40)
        facebookButton.customizeButton(with: #imageLiteral(resourceName: "facebookLogo"))
        vkontakteButton.customizeButton(with: #imageLiteral(resourceName: "vkLogo"))
        appleButton.customizeButton(with: #imageLiteral(resourceName: "appleLogo"))
        googleButton.customizeButton(with: #imageLiteral(resourceName: "googleLogo"))
    }
    
    private func goToApp() {
        let vc = MainTabController()
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
}

extension RegistrationViewController: AuthServiceDelegate {
    
    func authServiceSignIn(user: User) {
        saveUserLocally(user: user)
        if user.loginMethod == kVKONTAKTE {
            DispatchQueue.main.sync {
                goToApp()
            }
        } else {
            goToApp()
        }
    }
    
    func authServiceDidSignInFail(error: Error) {
        print("ERROR!!! :\(error.localizedDescription)")
    }
    
    func authServiceShouldShow(_ viewController: UIViewController) {
        present(viewController, animated: true, completion: nil)
    }

}

