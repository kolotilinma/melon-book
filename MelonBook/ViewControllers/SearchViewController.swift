//
//  SearchViewController.swift
//  MelonBook
//
//  Created by Михаил on 16.10.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit
import CoreData

class SearchViewController: UIViewController {
    
    //MARK: - Properties
    var recipes = [Recipe]()
    var collectionView: UICollectionView!
    var dataSource: UICollectionViewDiffableDataSource<Section, Recipe>!
    var context: NSManagedObjectContext!
    enum Section: Int, CaseIterable {
        case recipes
    }
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        createDataSource()
        hideKeyboardWhenTappedAround()
        self.recipes = loadAllRecipes() ?? []
        self.reloadData(with: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.navigationBar.barStyle = .default
        navigationController?.navigationBar.isHidden = true
    }
    
    //MARK: - Helpers
    private func setupCollectionView() {
        collectionView = UICollectionView(frame: view.bounds, collectionViewLayout: createCompositionLayout())
        collectionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        collectionView.backgroundColor = .white
        view.addSubview(collectionView)
        
        collectionView.register(SearchHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: SearchHeader.reuseId)
        collectionView.register(RecipeCell.self, forCellWithReuseIdentifier: RecipeCell.reuseId)
        collectionView.delegate = self
    }
    
    private func reloadData(with searchText: String?) {
        let filtered = recipes.filter { (recipe) -> Bool in
            guard let filter = searchText else { return true }
            if filter.isEmpty { return true }
            let lowercasedFilter = filter.lowercased()
            return recipe.name!.lowercased().contains(lowercasedFilter)
        }
        
        var snapshot = NSDiffableDataSourceSnapshot<Section, Recipe>()
        snapshot.appendSections([.recipes])
        snapshot.appendItems(filtered, toSection: .recipes)

        dataSource?.apply(snapshot, animatingDifferences: true)
    }
    
    private func loadAllRecipes() -> [Recipe]? {
        let fetchRequest: NSFetchRequest<Recipe> = Recipe.fetchRequest()
        do {
            let results = try context.fetch(fetchRequest)
            return results
        } catch let error as NSError {
            print(error.localizedDescription)
            return nil
        }
    }
}

// MARK: - Data Source
extension SearchViewController {
    
    private func createDataSource() {
        dataSource = UICollectionViewDiffableDataSource<Section, Recipe>(collectionView: collectionView, cellProvider: { (collectionView, indexPath, recipe) -> UICollectionViewCell? in
            guard let section = Section(rawValue: indexPath.section) else {
                fatalError("Unknown section kind")
            }
            
            switch section {
            case .recipes:
                return self.configure(collectionView: collectionView, cellType: RecipeCell.self, with: recipe, for: indexPath)
            }
        })
        
        dataSource?.supplementaryViewProvider = {
            collectionView, kind, indexPath in
            guard let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: SearchHeader.reuseId, for: indexPath) as? SearchHeader else { fatalError("Can not create new section header") }
            sectionHeader.delegate = self
            return sectionHeader
        }
    }
    
    
}

// MARK: - Setup Layout
extension SearchViewController {
    private func createCompositionLayout() -> UICollectionViewLayout {
        let layout = UICollectionViewCompositionalLayout { (sectionIndex, layoutEnvironment) -> NSCollectionLayoutSection? in
            guard let section = Section(rawValue: sectionIndex) else {
                fatalError("Unknown section kind")
            }
            switch section {
            case .recipes:
                return self.createUsersSection()
            }
        }
        let config = UICollectionViewCompositionalLayoutConfiguration()
        config.interSectionSpacing = 16
        layout.configuration = config
        return layout
    }
    
    private func createUsersSection() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
                                              heightDimension: .fractionalHeight(0.8))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                               heightDimension: .estimated(200))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitem: item, count: 2)
        let spacing = CGFloat(15)
        group.interItemSpacing = .fixed(spacing)
        let section = NSCollectionLayoutSection(group: group)
        section.interGroupSpacing = spacing
        section.contentInsets = NSDirectionalEdgeInsets(top: 15, leading: 15, bottom: 0, trailing: 15)
        
        let sectionHeader = createSectionHeader()
        section.boundarySupplementaryItems = [sectionHeader]
        return section
    }
    
    private func createSectionHeader() -> NSCollectionLayoutBoundarySupplementaryItem{
        let sectionHeaderSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
                                                       heightDimension: .estimated(150))
        let sectionHeader = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: sectionHeaderSize,
                                                                        elementKind: UICollectionView.elementKindSectionHeader,
                                                                        alignment: .top)
        return sectionHeader
    }
}


// MARK: - UICollectionViewDelegate
extension SearchViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let recipe = self.dataSource.itemIdentifier(for: indexPath) else { return }
        let vcRec = RecipeViewController()
        vcRec.recipe = recipe
        self.navigationController?.pushViewController(vcRec, animated: true)
    }
}

//MARK: - SearchHeaderDelegate
extension SearchViewController: SearchHeaderDelegate {
    func updateRecipes(with searchText: String?) {
        reloadData(with: searchText)
    }
}
