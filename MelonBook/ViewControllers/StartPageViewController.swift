//
//  StartPageViewController.swift
//  MelonBook
//
//  Created by Михаил on 29.09.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit
import CoreData

class StartPageViewController: UIViewController {
    
    //MARK: - Properties
    var context: NSManagedObjectContext!
    fileprivate let logoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = #imageLiteral(resourceName: "startImage")
        return imageView
    }()
    fileprivate let titleLabel = UILabel(text: "Melon Book",font: .hiragano(44), color: #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1))
    fileprivate let loginButton: UIButton = {
        let button  = UIButton(type: .system)
        button.setTitle("Вход", for: .normal)
        button.setTitleColor( #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1), for: .normal)
        button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.7004585519)
        button.layer.cornerRadius = 25
        button.titleLabel?.font = .ahellya(30)
        button.addTarget(self, action: #selector(handleLogin), for: .touchUpInside)
        return button
    }()
    var firstLoad: Bool?
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadUserDefaults()
    }
    
    //MARK: - Selectors
    @objc func handleLogin() {
        let tabController = RegistrationViewController()
        tabController.modalPresentationStyle = .fullScreen
        present(tabController, animated: true, completion: nil)
        
    }
    
    //MARK: - Helpers
    fileprivate func configureUI() {
        view.backgroundColor = .white
        navigationController?.navigationBar.isHidden = true
        view.addSubview(logoImageView)
        logoImageView.addConstraintsToFillView(view)
        view.addSubview(titleLabel)
        titleLabel.centerX(inView: view, topAnchor: view.topAnchor, paddingTop: 120)
        view.addSubview(loginButton)
        loginButton.centerX(inView: view)
        loginButton.anchor(left: view.leftAnchor, right: view.rightAnchor,
                           bottom: view.safeAreaLayoutGuide.bottomAnchor,
                           paddingLeft: 72, paddingRight: 72, paddingBottom: 22, height: 50)
    }
    
    func loadUserDefaults() {
        firstLoad = UserDefaults.standard.bool(forKey: UserDefaults.kFIRSTRUN)
        if !firstLoad! {
            UserDefaults.standard.set(true, forKey: UserDefaults.kFIRSTRUN)
            UserDefaults.standard.synchronize()
            getDataFromJson()
        }
    }
    
    func getDataFromJson() {
        if let fileLocation = Bundle.main.url(forResource: "myData", withExtension: "json") {
            print("FirstReadJson!!!!")
            // do catch in case of an error
            do {
                let data = try Data(contentsOf: fileLocation)
                let jsonDecoder = JSONDecoder()
                let dataFromJson = try jsonDecoder.decode([DishData].self, from: data)
                
//                let privateContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
//                privateContext.parent = CoreDataManager.shared.persistentContainer.viewContext
                
                dataFromJson.forEach { (dishDataJson) in
                    let dishData = DishGroup(context: context)
                    dishData.name = dishDataJson.name
                    let dishImage = UIImage(named: dishDataJson.photoName)
                    let dishImageData = dishImage!.pngData()
                    dishData.dishImageData = dishImageData
                    dishDataJson.recipes.forEach { (recipeJson) in
                        let recipe = Recipe(context: context)
                        recipe.dishGroup = dishData
                        recipe.name = recipeJson.name
                        recipe.numberOfServings = Int16(recipeJson.numberOfServings!)
                        recipe.time = Int16(recipeJson.time!)
                        recipe.difficult = Int16(recipeJson.difficult!)
                        recipe.teg = recipeJson.teg
                        recipe.comment = recipeJson.comment
                        guard let recipeImageName = recipeJson.titleImage else { return }
                        let recipeImage = UIImage(named: recipeImageName)
                        let recipeImageData = recipeImage!.pngData()
                        recipe.recipeImageData = recipeImageData
                        recipeJson.cookingStage?.forEach({ (cookingStageJson) in
                            let cookingStage = StepCooking(context: context)
                            cookingStage.recipe = recipe
                            cookingStage.number = Int16(cookingStageJson.number)
                            cookingStage.stepDescription = cookingStageJson.description
                            guard let stepImageName = cookingStageJson.image else { return }
                            let stepImage = UIImage(named: stepImageName)
                            let stepStageData = stepImage!.pngData()
                            cookingStage.stepImageData = stepStageData
                            do {
                                try context.save()
                            } catch let error as NSError {
                                print(error.localizedDescription)
                            }
                        })
                        recipeJson.section?.forEach({ (sectionJson) in
                            let section = Section(context: context)
                            section.recipe = recipe
                            section.name = sectionJson.name
                            sectionJson.ingredient.forEach { (ingredientJson) in
                                let ingredient = Ingredient(context: context)
                                ingredient.section = section
                                ingredient.name = ingredientJson.name
                                ingredient.amount = Int16(ingredientJson.amount!)
                                ingredient.unit = ingredientJson.unit
                                do {
                                    try context.save()
                                } catch let error as NSError {
                                    print(error.localizedDescription)
                                }
                            }
                            do {
                                try context.save()
                            } catch let error as NSError {
                                print(error.localizedDescription)
                            }
                        })
                    }
                    do {
                        try context.save()
//                        try privateContext.parent?.save()
                    } catch let error as NSError {
                        print(error.localizedDescription)
                    }
                }
                
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
    }
    
    
}

