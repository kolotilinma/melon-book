//
//  CustomTabBar.swift
//  MelonBook
//
//  Created by Евгения Колдышева on 09.09.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import Foundation
import UIKit

class CustomTabBar : UITabBar {
    override open func sizeThatFits(_ size: CGSize) -> CGSize {
        super.sizeThatFits(size)
        for item in super.items! {
            item.image? = item.image?.withBaselineOffset(fromBottom: 13) ?? UIImage()
        }
        var sizeThatFits = super.sizeThatFits(size)
        sizeThatFits.height -= 10
        
        return sizeThatFits
    }
}
