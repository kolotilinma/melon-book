//
//  NewRecipeCommentView.swift
//  MelonBook
//
//  Created by Михаил on 25.10.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

class NewRecipeCommentView: UIView {
    
    var textView: UITextView = {
        let textView = UITextView()
        textView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        textView.layer.cornerRadius = 12
        textView.layer.borderColor = UIColor(red: 0.933, green: 0.933, blue: 0.933, alpha: 1).cgColor
        textView.textContainerInset = UIEdgeInsets(top: 15, left: 5, bottom: 5, right: 5)
        textView.font = .ahellya(15)
        textView.textColor = #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1)
        textView.textAlignment = .left
        return textView
    }()
    fileprivate var label = UILabel(text: "макс./500", font: .ahellya(13), color: .placeholderText)
    fileprivate var placeholder = UILabel(text: "Комментарии", font: .ahellya(15), color: .placeholderText)
    
    init() {
        super.init(frame: .zero)
        textView.delegate = self
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        addSubview(textView)
        textView.addConstraintsToFillView(self)
        
        textView.addSubview(placeholder)
        placeholder.anchor(top: textView.topAnchor, left: textView.leftAnchor, paddingTop: 15, paddingLeft: 10)
        
        addSubview(label)
        label.anchor(right: textView.rightAnchor, bottom: textView.bottomAnchor,
                     paddingRight: 10, paddingBottom: 5)
    }
    
}

extension NewRecipeCommentView: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        let charCount = textView.text.count
        label.text = charCount == 0 ? "макс./500" : "\(charCount)/500"
        if charCount >= 500 {
            
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            placeholder.isHidden = true
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            placeholder.isHidden = false
        }
    }
    
}
