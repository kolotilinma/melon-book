//
//  NewRecipeFoldersTableView.swift
//  MelonBook
//
//  Created by Михаил on 26.10.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

protocol NewRecipeFoldersTableViewDelegate: class {
    func selectedFolder(folder: DishGroup)
}

class NewRecipeFoldersTableView: UIView {
    
    weak var delegate: NewRecipeFoldersTableViewDelegate?
    fileprivate var selectedCell: IndexPath = [0,0]
    fileprivate var foldersList: [DishGroup] = []
    fileprivate let titleLabel = UILabel(text: "Выбрать папку:", font: .ahellya(23), color: #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1))
    fileprivate let tableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .none
        tableView.backgroundColor = .white
        tableView.layer.cornerRadius = 12
        tableView.register(FolderCell.self, forCellReuseIdentifier: "FolderCell")
        return tableView
    }()
    
    init() {
        super.init(frame: .zero)
        foldersList = CoreDataManager.shared.fetchAllDishData()
        tableView.delegate = self
        tableView.dataSource = self
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        addSubview(titleLabel)
        titleLabel.anchor(top: self.topAnchor, left: self.leftAnchor)
        addSubview(tableView)
        tableView.anchor(top: titleLabel.bottomAnchor, left: self.leftAnchor,
                         right: self.rightAnchor, bottom: self.bottomAnchor,
                         paddingTop: 20)
    }
    
    func showNewFolderController() {
        let modalViewController = NewFolderViewController()
        modalViewController.modalPresentationStyle = .automatic
        modalViewController.delegate = self
        let myVC = UIApplication.topViewController()
        myVC?.present(modalViewController, animated: true, completion: nil)
    }
    
    func setupFirstFolder(with folder: DishGroup?) {
        if folder != nil {
            delegate?.selectedFolder(folder: folder!)
            
//            tableView.reloadData()
        } else {
            delegate?.selectedFolder(folder: foldersList[selectedCell.row])
        }
        
    }
    
}

extension NewRecipeFoldersTableView: NewFolderDelegate {
    func updateData() {
        foldersList = CoreDataManager.shared.fetchAllDishData()
        selectedCell = [0,foldersList.count - 1]
        tableView.reloadData()
    }
}


extension NewRecipeFoldersTableView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if foldersList.count == 0 {
            return 1
        } else {
            return foldersList.count + 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FolderCell", for: indexPath) as! FolderCell
        cell.selectionStyle = .none
        if indexPath.row == foldersList.count  {
            cell.folderLabel.text = "Новая папка"
            cell.dotImageView.image = #imageLiteral(resourceName: "Rectangle 113")
            return cell
        } else {
            cell.folderLabel.text = foldersList[indexPath.row].name
            
            if indexPath == selectedCell {
                cell.dotImageView.image = #imageLiteral(resourceName: "Rectangle 278")
            } else {
                cell.dotImageView.image = #imageLiteral(resourceName: "Rectangle 279")
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == foldersList.count  {
            tableView.deselectRow(at: indexPath, animated: true)
            showNewFolderController()
        } else {
            tableView.deselectRow(at: indexPath, animated: true)
            selectedCell = indexPath
            delegate?.selectedFolder(folder: foldersList[indexPath.row])
            tableView.reloadData()
        }
    }
    
}
