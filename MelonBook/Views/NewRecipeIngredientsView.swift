//
//  NewRecipeIngredientsView.swift
//  MelonBook
//
//  Created by Михаил on 27.10.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit
//import PickerTextField

protocol ItemTableViewCellDelegate: NSObjectProtocol {
    func textFieldDidEndEditing(textField: UITextField, cell: CreateIngredientCell)
    func textFieldDidEndEditing(textField: UITextField, indexSection: Int)
}

class NewRecipeIngredientsView: UIView {
    
    var ingridients: [SectionOld] = [
        SectionOld(name: "",
                   ingredient: [IngredientOld(name: "", amount: nil, unit: "Ед.")])]
    
    var unitsList = ["шт." , "г", "Кг", "мл", "ч.л.", "ст.л", "+ Добавить свое"]
    fileprivate let titleLabel = UILabel(text: "Ингредиенты:", font: .ahellya(23), color: #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1))
    let tableView: ContentSizedTableView = {
        let tableView = ContentSizedTableView()
        tableView.separatorStyle = .none
        tableView.isScrollEnabled = false
        tableView.backgroundColor = UIColor(red: 0.933, green: 0.933, blue: 0.933, alpha: 1)
        tableView.contentSize = CGSize(width: 0, height: 200)
        tableView.register(CreateSectionCell.self, forCellReuseIdentifier: "CreateSectionCell")
        tableView.register(CreateIngredientCell.self, forCellReuseIdentifier: "CreateIngredientCell")
        return tableView
    }()
    
    private lazy var addSectionButton = UIButton(title: "Добавить раздел", font: .ahellya(13))
    private lazy var addIngredientButton = UIButton(title: "Добавить ингредиент", font: .ahellya(13))
    
    init() {
        super.init(frame: .zero)
        addSectionButton.addTarget(self, action: #selector(addSectionButtonTapped), for: .touchUpInside)
        addIngredientButton.addTarget(self, action: #selector(addIngredientButtonTapped), for: .touchUpInside)
        tableView.isEditing = true
        tableView.delegate = self
        tableView.dataSource = self
        tableView.dragInteractionEnabled = true
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 86.0
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        addSubview(titleLabel)
        titleLabel.anchor(top: self.topAnchor, left: self.leftAnchor, paddingLeft: 25)
        
        addSubview(tableView)
        tableView.anchor(top: titleLabel.bottomAnchor, left: self.leftAnchor,
                         right: self.rightAnchor, paddingTop: 20)
        
        let addButtonsView = UIStackView(arrangedSubviews: [addIngredientButton, addSectionButton],
                                         axis: .horizontal, spacing: 15)
        addButtonsView.distribution = .fillEqually
        addSubview(addButtonsView)
        addButtonsView.anchor(top: tableView.bottomAnchor, left: self.leftAnchor,
                              right: self.rightAnchor, bottom: self.bottomAnchor,
                              paddingTop: 15, paddingLeft: 25, paddingRight: 25)
        addIngredientButton.anchor(height: 63)
        addSectionButton.anchor(height: 63)
        
    }
    
    
    @objc func addSectionButtonTapped(){
        ingridients.append(SectionOld(name: "", ingredient: [IngredientOld(name: "", amount: nil, unit: "Ед.")]))
        tableView.reloadData()
    }
    
    @objc func addIngredientButtonTapped(){
        if ingridients.count == 0 {
            addSectionButtonTapped()
        } else {
            ingridients[ingridients.count - 1].ingredient.append(IngredientOld(name: "", amount: nil, unit: "Ед."))
            tableView.reloadData()
        }
    }
    
    
}

extension NewRecipeIngredientsView: PickerTextFieldDataSource {
    func numberOfRows(in pickerTextField: PickerTextField) -> Int {
        return unitsList.count
    }
    
    func pickerTextField(_ pickerTextField: PickerTextField, titleForRow row: Int) -> String? {
        return unitsList[row]
    }
    
    
}

extension NewRecipeIngredientsView: ItemTableViewCellDelegate {
    func textFieldDidEndEditing(textField: UITextField, indexSection: Int) {
        guard let text = textField.text else { return }
        ingridients[indexSection].name = text
    }
    
    func textFieldDidEndEditing(textField: UITextField, cell: CreateIngredientCell) {
        guard let indexPath = tableView.indexPath(for: cell) else { return }
        if textField == cell.ingredientTextField {
            guard let text = textField.text else { return }
            ingridients[indexPath.section].ingredient[indexPath.row].name = text
        } else if textField == cell.countTextField {
            guard let text = textField.text else { return }
            let amount = (text as NSString).integerValue
            ingridients[indexPath.section].ingredient[indexPath.row].amount = amount
        } else if textField == cell.unitTextField {
            guard let text = textField.text else { return }
            ingridients[indexPath.section].ingredient[indexPath.row].unit = text
        }
        print(ingridients)
    }
    
}


extension NewRecipeIngredientsView: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return ingridients.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 86
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = CreateSectionView()
        headerView.delegate = self
        headerView.section = section
        headerView.sectionNameTextField.text = ingridients[section].name
        headerView.sectionNameTextField.placeholder = "Раздел \(section + 1)"
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ingridients[section].ingredient.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CreateIngredientCell", for: indexPath) as! CreateIngredientCell
        cell.selectionStyle = .none
        cell.delegate = self
        cell.unitTextField.dataSource = self
        cell.ingridient = ingridients[indexPath.section].ingredient[indexPath.row]
        cell.ingredientTextField.placeholder = "Ингредиент \(indexPath.row + 1)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            ingridients[indexPath.section].ingredient.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .right)
            if ingridients[indexPath.section].ingredient.count == 0 {
                ingridients.remove(at: indexPath.section)
                tableView.deleteSections([indexPath.section], with: .fade)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let movedObject = self.ingridients[sourceIndexPath.section].ingredient[sourceIndexPath.row]
        ingridients[sourceIndexPath.section].ingredient.remove(at: sourceIndexPath.row)
        ingridients[destinationIndexPath.section].ingredient.insert(movedObject, at: destinationIndexPath.row)

    }

    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }

    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
}
