//
//  NewRecipeStepView.swift
//  MelonBook
//
//  Created by Михаил on 26.10.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

protocol CellImageTapDelegate {
    func tableCell(didClickedImageOf tableCell: UITableViewCell)
}

class NewRecipeStepView: UIView {
    
    var steps: [[String: UIImage]] = [["":#imageLiteral(resourceName: "Camera")]]
    let titleLabel = UILabel(text: "Приготовление:", font: .ahellya(23), color: #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1))
    let tableView: UITableView = {
        let tableView = ContentSizedTableView()
        tableView.separatorStyle = .none
        tableView.isScrollEnabled = false
        tableView.backgroundColor = UIColor(red: 0.933, green: 0.933, blue: 0.933, alpha: 1)
        tableView.contentSize = CGSize(width: 0, height: 200)
        tableView.register(StepCell.self, forCellReuseIdentifier: "StepCell")
        return tableView
    }()
    fileprivate let plusImageView = UIImageView(image: #imageLiteral(resourceName: "Rectangle 113"), contentMode: .scaleAspectFit)
    fileprivate var addStepTextView: UITextView = {
        let textView = UITextView()
        textView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        textView.layer.cornerRadius = 12
        textView.layer.borderWidth = 3
        textView.layer.borderColor = UIColor(red: 0.933, green: 0.933, blue: 0.933, alpha: 1).cgColor
        textView.textContainerInset = UIEdgeInsets(top: 10, left: 5, bottom: 5, right: 5)
        textView.text = "Cледующий шаг"
        textView.font = .ahellya(15)
        textView.textColor = #colorLiteral(red: 0.768627451, green: 0.768627451, blue: 0.768627451, alpha: 1)
        textView.textAlignment = .left
        textView.isUserInteractionEnabled = true
        return textView
    }()
    
    init() {
        super.init(frame: .zero)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 95.0
        setupView()
        setupActions()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        addSubview(titleLabel)
        titleLabel.anchor(top: self.topAnchor, left: self.leftAnchor)
        
        addSubview(tableView)
        tableView.anchor(top: titleLabel.bottomAnchor, left: self.leftAnchor,
                         right: self.rightAnchor,//, bottom: self.bottomAnchor,
                         paddingTop: 20)
        
        addSubview(plusImageView)
        plusImageView.anchor(top: tableView.bottomAnchor, left: tableView.leftAnchor, paddingTop: 21, paddingLeft: 7, width: 20, height: 20)
        
        addSubview(addStepTextView)
        addStepTextView.anchor(top: tableView.bottomAnchor, left: plusImageView.rightAnchor, right: self.rightAnchor, bottom: self.bottomAnchor, paddingTop: 21, paddingLeft: 22, paddingBottom: 5, height: 64)
    }
    
    private func setupActions() {
        let addPlusStepGesture = UITapGestureRecognizer(target: self, action: #selector(addStepViewTapped))
        plusImageView.isUserInteractionEnabled = true
        plusImageView.addGestureRecognizer(addPlusStepGesture)
        
        let addStepGesture = UITapGestureRecognizer(target: self, action: #selector(addStepViewTapped))
        addStepTextView.addGestureRecognizer(addStepGesture)
    }
    
    @objc func addStepViewTapped(){
        steps.append(["":#imageLiteral(resourceName: "Camera")])
        tableView.reloadData()
    }
    
}

extension NewRecipeStepView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return steps.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StepCell", for: indexPath) as! StepCell
        cell.stepLabel.text = "Шаг \(indexPath.row + 1)"
        if steps[indexPath.row].keys.first != "" {
            cell.placeholder.isHidden = true
            cell.stepTextView.text = steps[indexPath.row].keys.first
        }
        
        cell.cameraImageView.image = steps[indexPath.row].values.first
        cell.selectionStyle = .none
        cell.delegate = self
        return cell
    }
        
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if steps.count > 0 {
            let cell = tableView.cellForRow(at: indexPath) as! StepCell
            cell.clearField()
            steps.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            tableView.reloadData()
        } else {
            guard let controller = UIApplication.topViewController() else { return }
            controller.showAlert(with: "Error", and: "You can't remove last section")
        }
    }
    
}

extension NewRecipeStepView:  CellImageTapDelegate {
    func tableCell(didClickedImageOf tableCell: UITableViewCell) {
        if let rowIndexPath = tableView.indexPath(for: tableCell) {
            print("Row Selected of indexPath: \(rowIndexPath)")
            guard let controller = UIApplication.topViewController() else { return }
            ImagePickerManager().pickImage(controller){ image in
//                self.steps[rowIndexPath.row].values.first = image
//                self.tableView.reloadData()
                let cell = self.tableView.cellForRow(at: rowIndexPath) as! StepCell
                cell.cameraImageView.image = image
            }

        }
    }
}
