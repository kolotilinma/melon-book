//
//  NewRecipeTagsView.swift
//  MelonBook
//
//  Created by Михаил on 26.10.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

class NewRecipeTagsView: UIView {
        
    var label: UILabel = {
        var label = UILabel(text: "", font: .ahellya(13), color: #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1))
        label.textAlignment = .left
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        return label
    }()
    
    fileprivate let textField: UITextField = {
        let textField = UITextField()
        textField.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        textField.placeholder = "Добавить тег"
        textField.font = .ahellya(15)
        textField.layer.cornerRadius = 12
        textField.contentVerticalAlignment = .center
        textField.textAlignment = .justified
        textField.setLeftPaddingPoints(15)
        return textField
    }()
    
    fileprivate let imageView = UIImageView(image: #imageLiteral(resourceName: "Rectangle 113"), contentMode: .scaleAspectFit)
    
    init() {
        super.init(frame: .zero)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(addTag))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(gesture)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        addSubview(label)
        label.anchor(top: self.topAnchor, left: self.leftAnchor, right: self.rightAnchor)
        
        addSubview(imageView)
        imageView.anchor(left: self.leftAnchor, paddingLeft: 7, width: 20, height: 20)
        
        addSubview(textField)
        textField.anchor(left: imageView.rightAnchor, right: self.rightAnchor,
                         bottom: self.bottomAnchor, paddingLeft: 20, height: 50)
        
        imageView.centerY(inView: textField)
    }
    
    @objc func addTag(){
        if textField.text != "" && textField.text != nil {
            label.text?.append("#\(String(textField.text!)) ")
            textField.text = ""
        }
    }
    
}
