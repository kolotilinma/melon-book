//
//  NewRecipeTitleElemView.swift
//  MelonBook
//
//  Created by Михаил on 24.10.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

class NewRecipeTitleElemView: UIView {
    
    let numOfServList = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]

    fileprivate let label = UILabel(text: "", font: .ahellya(13), color: #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1))
    fileprivate let imageView = UIImageView(image: UIImage(), contentMode: .scaleAspectFit)
    let servTextField: PickerTextField = {
        let textField = PickerTextField()
        textField.backgroundColor = .white
//        textField.placeholder = "1"
        textField.font = .ahellya(13)
        textField.textColor = #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1)
        textField.layer.cornerRadius = 12
        textField.contentVerticalAlignment = .center
        textField.keyboardType = .numberPad
        textField.textAlignment = .justified
        textField.setLeftPaddingPoints(55)
        return textField
    }()
    
    init(text: String, image: UIImage, placeholder: String, leftPadding: CGFloat) {
        super.init(frame: .zero)
        label.text = text
        imageView.image = image
        imageView.setupColor(color: .placeholderText)
        servTextField.placeholder = placeholder
        servTextField.setLeftPaddingPoints(leftPadding)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        addSubview(label)
        label.centerY(inView: self, leftAnchor: self.leftAnchor, paddingLeft: 10)
        
        addSubview(servTextField)
        servTextField.centerY(inView: self)
        servTextField.anchor(top: self.topAnchor, right: self.rightAnchor, bottom: self.bottomAnchor)
        servTextField.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.5).isActive = true
        
        addSubview(imageView)
        imageView.centerY(inView: self)
        imageView.anchor(right: self.rightAnchor, paddingRight: 10,  width: 30, height: 30)
        
        servTextField.dataSource = self
    }
    
}

extension NewRecipeTitleElemView: PickerTextFieldDataSource {
    func numberOfRows(in pickerTextField: PickerTextField) -> Int {
        return numOfServList.count
    }
    
    func pickerTextField(_ pickerTextField: PickerTextField, titleForRow row: Int) -> String? {
        return numOfServList[row]
    }
}
