//
//  RecipeHeaderElementView.swift
//  MelonBook
//
//  Created by Михаил on 23.10.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

class RecipeHeaderElementView: UIView {

    let valueLabel = UILabel(text: "", font: .ahellya(18), color: #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1))
    fileprivate let label = UILabel(text: "", font: .ahellya(13), color: #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1))
    fileprivate let imageView = UIImageView(image: UIImage(), contentMode: .scaleAspectFit)
    
    init(text: String, image: UIImage, value: String) {
        super.init(frame: .zero)
        label.text = text
        label.textAlignment = .center
        imageView.image = image
        imageView.setupColor(color: #colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9333333333, alpha: 1))
        valueLabel.text = value
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        self.layer.cornerRadius = 10
        self.layer.borderWidth = 1
        self.layer.borderColor = #colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9333333333, alpha: 1)
        addSubview(label)
        label.anchor(left: self.leftAnchor, right: self.rightAnchor, bottom: self.bottomAnchor, paddingBottom: 5)
        addSubview(imageView)
        imageView.centerX(inView: self, topAnchor: self.topAnchor, paddingTop: 10, offset: 20)
        imageView.setDimensions(width: 30, height: 30)
        addSubview(valueLabel)
        valueLabel.centerY(inView: imageView)
        valueLabel.anchor(right: imageView.leftAnchor, paddingRight: 5)
    }
    

}
