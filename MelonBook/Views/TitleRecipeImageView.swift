//
//  TitleRecipeImageView.swift
//  MelonBook
//
//  Created by Михаил on 24.10.2020.
//  Copyright © 2020 Евгения Кирюшина. All rights reserved.
//

import UIKit

class TitleRecipeImageView: UIView {

    var label = UILabel(text: "", font: .ahellya(13), color: #colorLiteral(red: 0.3254901961, green: 0.3215686275, blue: 0.3215686275, alpha: 1))
    var imageView = UIImageView(image: #imageLiteral(resourceName: "Rectangle 83"), contentMode: .scaleAspectFill)
    var iconImage = UIImageView(image: #imageLiteral(resourceName: "Camera"), contentMode: .scaleAspectFit)
    
    init(title: String, image: UIImage, iconsIsHidden: Bool = false) {
        super.init(frame: .zero)
        label.text = title
        label.textAlignment = .center
        imageView.image = image
        iconImage.isHidden = iconsIsHidden
        label.isHidden = iconsIsHidden
        if !iconsIsHidden {
            let gesture = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
            imageView.isUserInteractionEnabled = true
            imageView.addGestureRecognizer(gesture)
        }
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        self.layer.cornerRadius = 20
        self.clipsToBounds = true
        
        
        addSubview(imageView)
        imageView.anchor(top: self.topAnchor, left: self.leftAnchor, right: self.rightAnchor, bottom: self.bottomAnchor)
        imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor, multiplier: 0.7).isActive = true
        imageView.alpha = 0.3
        
        addSubview(iconImage)
        iconImage.center(inView: imageView)
        iconImage.anchor(width: 40, height: 40)
        
        addSubview(label)
        label.centerX(inView: imageView, topAnchor: iconImage.bottomAnchor)
    }
    
    @objc func imageTapped(){
        guard let controller = UIApplication.topViewController() else { return }
        ImagePickerManager().pickImage(controller){ image in
            self.imageView.image = image
            self.imageView.alpha = 1
            self.iconImage.isHidden = true
            self.label.isHidden = true
        }
    }
    

}
